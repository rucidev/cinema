package com.ikubinfo.cinema.model.movie.movieschedule;

import com.ikubinfo.cinema.commons.date.ASUtilDate;
import com.ikubinfo.cinema.commons.entity.Entity;
import com.ikubinfo.cinema.model.monitor.Monitor;
import com.ikubinfo.cinema.model.movie.Movie;
import com.ikubinfo.cinema.model.user.User;

public class MovieSchedule extends Entity {
    private Monitor monitor = null;
    private Movie movie = null;
    private double price;
    private ASUtilDate startTime = null;
    private ASUtilDate endTime = null;
    private User cUser = null;
    private User mUser = null;

    public MovieSchedule(){ }

    public MovieSchedule(int id){
        super(id);
    }

    public Monitor getMonitor() {
        return monitor;
    }

    public void setMonitor(Monitor monitor) {
        this.monitor = monitor;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public ASUtilDate getStartTime() {
        return startTime;
    }

    public void setStartTime(ASUtilDate startTime) {
        this.startTime = startTime;
    }

    public ASUtilDate getEndTime() {
        return endTime;
    }

    public void setEndTime(ASUtilDate endTime) {
        this.endTime = endTime;
    }

    public User getcUser() {
        return cUser;
    }

    public void setcUser(User cUser) {
        this.cUser = cUser;
    }

    public User getmUser() {
        return mUser;
    }

    public void setmUser(User mUser) {
        this.mUser = mUser;
    }

    @Override
    public String toString() {
        return "MovieSchedule{" +
                "id=" + getId() +
                ", monitor=" + monitor +
                ", movie=" + movie +
                ", price=" + price +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                '}';
    }
}
