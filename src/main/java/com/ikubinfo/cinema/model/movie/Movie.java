package com.ikubinfo.cinema.model.movie;

import com.ikubinfo.cinema.commons.date.ASUtilDate;
import com.ikubinfo.cinema.commons.date.ASUtilTime;
import com.ikubinfo.cinema.commons.entity.Entity;
import com.ikubinfo.cinema.model.user.User;

public class Movie extends Entity {
    private EMovieType movieType = null;
    private String title = null;
    private String producer = null;
    private double price;
    private ASUtilTime duration = null;
    private ASUtilDate startTime = null;
    private ASUtilDate endTime = null;
    private User cUser = null;
    private User mUser = null;

    public Movie(){ }

    public Movie(int id){
        super(id);
    }

    public EMovieType getMovieType() {
        return movieType;
    }

    public void setMovieType(EMovieType movieType) {
        this.movieType = movieType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public ASUtilDate getStartTime() {
        return startTime;
    }

    public void setStartTime(ASUtilDate startTime) {
        this.startTime = startTime;
    }

    public ASUtilDate getEndTime() {
        return endTime;
    }

    public void setEndTime(ASUtilDate endTime) {
        this.endTime = endTime;
    }

    public ASUtilTime getDuration() {
        return duration;
    }

    public void setDuration(ASUtilTime duration) {
        this.duration = duration;
    }

    public User getcUser() {
        return cUser;
    }

    public void setcUser(User cUser) {
        this.cUser = cUser;
    }

    public User getmUser() {
        return mUser;
    }

    public void setmUser(User mUser) {
        this.mUser = mUser;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "id=" + getId() +
                ", movieType=" + movieType +
                ", title='" + title + '\'' +
                ", producer='" + producer + '\'' +
                ", duration=" + duration.toMySqlTime() +
                '}';
    }
}
