package com.ikubinfo.cinema.model.movie.movieschedule;

import com.ikubinfo.cinema.commons.entity.EntityFilter;

public class MovieScheduleFilter extends EntityFilter {
    private int monitorID = MINUS_ONE;
    private int movieID = MINUS_ONE;
    private boolean isFullBody = true;

    public MovieScheduleFilter(){ }

    public MovieScheduleFilter(boolean isFullBody){
        this.isFullBody = isFullBody;
    }

    public MovieScheduleFilter(int id, boolean isFullBody){
        super(id);
        this.isFullBody = isFullBody;
    }

    public boolean isFullBody() {
        return isFullBody;
    }

    public void setFullBody(boolean fullBody) {
        isFullBody = fullBody;
    }

    public int getMonitorID() {
        return monitorID;
    }

    public void setMonitorID(int monitorID) {
        this.monitorID = monitorID;
    }

    public int getMovieID() {
        return movieID;
    }

    public void setMovieID(int movieID) {
        this.movieID = movieID;
    }
}
