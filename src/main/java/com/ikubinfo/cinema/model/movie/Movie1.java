package com.ikubinfo.cinema.model.movie;

import com.ikubinfo.cinema.model.User1Entity;

import javax.persistence.*;

/**
 * Created by Aleks on Jun 09, 2018
 */
@Entity
@Table(name = "movie1", schema = "sql7235211", catalog = "")
public class Movie1 {
    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false)
    private int id;
    @Basic
    @Column(name = "name", nullable = false, length = 250)
    private String name;

    public void Movie1(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
