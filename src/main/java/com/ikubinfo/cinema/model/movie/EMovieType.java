package com.ikubinfo.cinema.model.movie;

import java.util.*;

/**
 * Created by Aleks on May 26, 2018
 */
public enum EMovieType {
    BIOGRAPHY(Constants.BIOGRAPHY, "Biography"),
    PSYCHOLOGY(Constants.PSYCHOLOGY, "Psychology"),
    ACTION(Constants.ACTION, "Action"),
    ROMANCE(Constants.ROMANCE, "Romance"),
    COMEDY(Constants.COMEDY, "Comedy"),
    DRAMA(Constants.DRAMA, "Drama"),
    THRILLER(Constants.THRILLER, "Thriller"),
    HORROR(Constants.HORROR, "Horror"),
    ANIMATION(Constants.ANIMATION, "Animation"),
    CRIME(Constants.CRIME, "Crime"),
    SPORT(Constants.SPORT, "Sport"),
    ADVENTURE(Constants.ADVENTURE, "Adventure"),
    HINDI(Constants.HINDI, "Hindi");

    private int id;
    private String name;

    private static Map<Integer, EMovieType> idToString;


    EMovieType(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static List<EMovieType> getMovieTypes(){
        return new ArrayList<EMovieType>(Arrays.asList(values()));
    }

    public static EMovieType getEnumByID(int id) {
        if (idToString == null) {
            initMapping();
        }
        return idToString.get(id);
    }

    private static void initMapping() {
        idToString = new HashMap<Integer, EMovieType>();
        for (EMovieType m : values()) {
            idToString.put(m.id, m);
        }
    }

    public class Constants{
        public static final int BIOGRAPHY = 1;
        public static final int PSYCHOLOGY = 2;
        public static final int ACTION = 3;
        public static final int ROMANCE = 4;
        public static final int COMEDY = 5;
        public static final int DRAMA = 6;
        public static final int THRILLER = 7;
        public static final int HORROR = 8;
        public static final int ANIMATION = 9;
        public static final int CRIME = 10;
        public static final int SPORT = 11;
        public static final int ADVENTURE = 12;
        public static final int HINDI = 13;
    }
}
