package com.ikubinfo.cinema.model.movie;

import com.ikubinfo.cinema.commons.entity.EntityFilter;

public class MovieFilter extends EntityFilter {
    private int movieTypeID = MINUS_ONE;
    private String title = null;
    private String producer = null;
    private boolean isFullBody = true;

    public MovieFilter(boolean isFullBody){
        this.isFullBody = isFullBody;
    }

    public MovieFilter(){ }

    public MovieFilter(int id, boolean isFullBody){
        super(id);
        this.isFullBody = isFullBody;
    }

    public MovieFilter(String title, boolean isFullBody){
        this.title = title;
        this.isFullBody = isFullBody;
    }

    public boolean isFullBody() {
        return isFullBody;
    }

    public void setFullBody(boolean fullBody) {
        isFullBody = fullBody;
    }

    public int getMovieTypeID() {
        return movieTypeID;
    }

    public void setMovieTypeID(int movieTypeID) {
        this.movieTypeID = movieTypeID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }
}


