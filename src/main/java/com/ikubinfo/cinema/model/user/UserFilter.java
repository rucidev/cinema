package com.ikubinfo.cinema.model.user;

import com.ikubinfo.cinema.commons.entity.EntityFilter;

public class UserFilter extends EntityFilter {
    private int id = MINUS_ONE;
    private String username = null;
    private boolean isFullBody = true;

    public UserFilter(){ }

    public UserFilter(boolean isFullBody) throws Exception{
        this.isFullBody = isFullBody;
    }

    public UserFilter(String username, boolean isFullBody){
        this.username = username;
        this.isFullBody = isFullBody;
    }

    public UserFilter(int id, boolean isFullBody){
        super(id);
        this.isFullBody = isFullBody;
    }

    public boolean isFullBody() {
        return isFullBody;
    }

    public void setFullBody(boolean fullBody) {
        isFullBody = fullBody;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
