package com.ikubinfo.cinema.model.user;


import com.ikubinfo.cinema.commons.entity.Entity;

public class User extends Entity {
    private String name = null;
    private String surname = null;
    private String username = null;
    private String password = null;
    private String mobile = null;
    private String sex = null;

    public User(){ }

    public User(int id){
        super(id);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @Override
    public String toString() {
        return "com.cinema.User{" +
                "username='" + username + '\'' +
                '}';
    }
}
