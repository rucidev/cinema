package com.ikubinfo.cinema.model.administrator;

import com.ikubinfo.cinema.commons.entity.Entity;
import com.ikubinfo.cinema.model.user.User;

public class Administrator extends Entity {
    private User user;

    public Administrator(){ }

    public Administrator(int id){
        super(id);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
