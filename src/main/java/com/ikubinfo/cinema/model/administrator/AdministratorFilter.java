package com.ikubinfo.cinema.model.administrator;

import com.ikubinfo.cinema.commons.entity.EntityFilter;

public class AdministratorFilter extends EntityFilter {
    private boolean isFullBody = true;
    private int userID = MINUS_ONE;

    public AdministratorFilter(){ }

    public AdministratorFilter(boolean isFullBody){
        this.isFullBody = isFullBody;
    }

    public AdministratorFilter(int id, boolean isFullBody){
        super(id);
        this.isFullBody = isFullBody;
    }

    public boolean isFullBody() {
        return isFullBody;
    }

    public void setFullBody(boolean fullBody) {
        isFullBody = fullBody;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }
}
