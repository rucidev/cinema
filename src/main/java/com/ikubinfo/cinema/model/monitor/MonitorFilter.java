package com.ikubinfo.cinema.model.monitor;

import com.ikubinfo.cinema.commons.date.ASUtilDate;
import com.ikubinfo.cinema.commons.entity.EntityFilter;

public class MonitorFilter extends EntityFilter {
    private int roomID = MINUS_ONE;
    private ASUtilDate time = null;
    private boolean isFullBody = true;

    public MonitorFilter(){ }

    public MonitorFilter(boolean isFullBody) throws Exception{
        this.isFullBody = isFullBody;
        this.time = new ASUtilDate();
    }

    public MonitorFilter(String time, boolean isFullBody) throws Exception{
        this.time = new ASUtilDate();
        this.isFullBody = isFullBody;
    }

    public MonitorFilter(int id, boolean isFullBody) throws Exception{
        super(id);
        this.time = new ASUtilDate();
        this.isFullBody = isFullBody;
    }

    public boolean isFullBody() {
        return isFullBody;
    }

    public void setFullBody(boolean fullBody) {
        isFullBody = fullBody;
    }

    public int getRoomID() {
        return roomID;
    }

    public void setRoomID(int roomID) {
        this.roomID = roomID;
    }

    public ASUtilDate getTime() {
        return time;
    }

    public void setTime(ASUtilDate time) {
        this.time = time;
    }
}
