package com.ikubinfo.cinema.model.monitor;

import com.ikubinfo.cinema.commons.entity.Entity;
import com.ikubinfo.cinema.model.chair.Chair;
import com.ikubinfo.cinema.model.movie.Movie;
import com.ikubinfo.cinema.model.user.User;

import java.util.ArrayList;

public class Monitor extends Entity {
    private int monitorNumber;
    private int roomID;
    private Movie movie = null;
    private ArrayList<Chair> chairs = null;
    private double height;
    private double width;
    private User cUser = null;
    private User mUser = null;

    public Monitor(){ }

    public Monitor(int id){
        super(id);
    }

    public int getMonitorNumber() {
        return monitorNumber;
    }

    public void setMonitorNumber(int monitorNumber) {
        this.monitorNumber = monitorNumber;
    }

    public int getRoomID() {
        return roomID;
    }

    public void setRoomID(int roomID) {
        this.roomID = roomID;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public ArrayList<Chair> getChairs() {
        return chairs;
    }

    public void setChairs(ArrayList<Chair> chairs) {
        this.chairs = chairs;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public User getcUser() {
        return cUser;
    }

    public void setcUser(User cUser) {
        this.cUser = cUser;
    }

    public User getmUser() {
        return mUser;
    }

    public void setmUser(User mUser) {
        this.mUser = mUser;
    }

    @Override
    public String toString() {
        return "Monitor{" +
                "id=" + getId() +
                ", monitorNumber=" + monitorNumber +
                ", roomID=" + roomID +
//                ", chairs=" + chairs +
                ", height=" + height +
                ", width=" + width +
                '}';
    }
}
