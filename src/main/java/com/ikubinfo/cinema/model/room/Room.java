package com.ikubinfo.cinema.model.room;

import com.ikubinfo.cinema.commons.entity.Entity;
import com.ikubinfo.cinema.model.chair.Chair;
import com.ikubinfo.cinema.model.monitor.Monitor;

import java.util.ArrayList;

public class Room extends Entity {
    private String name = null;
    private ArrayList<Monitor> monitors = null;
    private ArrayList<Chair> chairs = null;

    public Room(){ }

    public Room(int id){
        super(id);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Monitor> getMonitors() {
        return monitors;
    }

    public void setMonitors(ArrayList<Monitor> monitors) {
        this.monitors = monitors;
    }

    public ArrayList<Chair> getChairs() {
        return chairs;
    }

    public void setChairs(ArrayList<Chair> chairs) {
        this.chairs = chairs;
    }
}
