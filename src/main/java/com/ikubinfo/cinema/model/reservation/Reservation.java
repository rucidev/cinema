package com.ikubinfo.cinema.model.reservation;

import com.ikubinfo.cinema.commons.entity.Entity;
import com.ikubinfo.cinema.model.chair.configuredchair.ConfiguredChair;
import com.ikubinfo.cinema.model.customer.Customer;

import java.util.ArrayList;

public class Reservation extends Entity {
    private Customer customer = null;
    private ArrayList<ConfiguredChair> configuredChairs = null;
    private double totalPrice = MINUS_ONE;

    public Reservation() { }

    public Reservation(int id) {
        super(id);
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public ArrayList<ConfiguredChair> getConfiguredChairs() {
        return configuredChairs;
    }

    public void setConfiguredChairs(ArrayList<ConfiguredChair> configuredChairs) {
        this.configuredChairs = configuredChairs;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }
}
