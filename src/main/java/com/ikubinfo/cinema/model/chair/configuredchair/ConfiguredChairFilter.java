package com.ikubinfo.cinema.model.chair.configuredchair;

import com.ikubinfo.cinema.commons.entity.EntityFilter;

public class ConfiguredChairFilter extends EntityFilter {
    private boolean isFullBody = true;

    public ConfiguredChairFilter(){ }

    public ConfiguredChairFilter(boolean isFullBody){
        this.isFullBody = isFullBody;
    }

    public ConfiguredChairFilter(int id, boolean isFullBody){
        super(id);
        this.isFullBody = isFullBody;
    }

    public boolean isFullBody() {
        return isFullBody;
    }

    public void setFullBody(boolean fullBody) {
        isFullBody = fullBody;
    }
}
