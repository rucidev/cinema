package com.ikubinfo.cinema.model.chair;

import com.ikubinfo.cinema.commons.entity.Entity;
import com.ikubinfo.cinema.model.administrator.Administrator;
import com.ikubinfo.cinema.model.customer.Customer;
import com.ikubinfo.cinema.model.user.User;

public class Chair extends Entity {
    private int chairNumber;
    private int roomID;
    private int monitorID;
    private Administrator administrator;
    private Customer customer;
    private boolean isBooked;
    private boolean isVip;
    private User cUser = null;
    private User mUser = null;

    public Chair(){ }

    public Chair(int id){
        super(id);
    }

    public int getMonitorID() {
        return monitorID;
    }

    public void setMonitorID(int monitorID) {
        this.monitorID = monitorID;
    }

    public int getChairNumber() {
        return chairNumber;
    }

    public void setChairNumber(int chairNumber) {
        this.chairNumber = chairNumber;
    }

    public boolean isVip() {
        return isVip;
    }

    public void setVip(boolean vip) {
        isVip = vip;
    }

    public Administrator getAdministrator() {
        return administrator;
    }

    public void setAdministrator(Administrator administrator) {
        this.administrator = administrator;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public boolean isBooked() {
        return isBooked;
    }

    public void setBooked(boolean booked) {
        isBooked = booked;
    }

    public User getcUser() {
        return cUser;
    }

    public void setcUser(User cUser) {
        this.cUser = cUser;
    }

    public User getmUser() {
        return mUser;
    }

    public void setmUser(User mUser) {
        this.mUser = mUser;
    }

    public int getRoomID() {
        return roomID;
    }

    public void setRoomID(int roomID) {
        this.roomID = roomID;
    }
}
