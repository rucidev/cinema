package com.ikubinfo.cinema.model.chair.configuredchair;

import com.ikubinfo.cinema.commons.entity.Entity;
import com.ikubinfo.cinema.model.administrator.Administrator;
import com.ikubinfo.cinema.model.chair.Chair;
import com.ikubinfo.cinema.model.customer.Customer;
import com.ikubinfo.cinema.model.monitor.Monitor;
import com.ikubinfo.cinema.model.movie.Movie;
import com.ikubinfo.cinema.model.movie.movieschedule.MovieSchedule;
import com.ikubinfo.cinema.model.user.User;

public class ConfiguredChair extends Entity {
    private Chair chair = null;
    private Monitor monitor = null;
    private Movie movie = null;
    private MovieSchedule movieSchedule = null;
    private Administrator administrator = null;
    private Customer customer = null;
    private boolean isBooked;
    private User cUser = null;
    private User mUser = null;

    public ConfiguredChair(){ }

    public ConfiguredChair(int id){
        super(id);
    }

    public Chair getChair() {
        return chair;
    }

    public void setChair(Chair chair) {
        this.chair = chair;
    }

    public MovieSchedule getMovieSchedule() {
        return movieSchedule;
    }

    public void setMovieSchedule(MovieSchedule movieSchedule) {
        this.movieSchedule = movieSchedule;
    }

    public Administrator getAdministrator() {
        return administrator;
    }

    public void setAdministrator(Administrator administrator) {
        this.administrator = administrator;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public boolean isBooked() {
        return isBooked;
    }

    public void setBooked(boolean booked) {
        isBooked = booked;
    }

    public Monitor getMonitor() {
        return monitor;
    }

    public void setMonitor(Monitor monitor) {
        this.monitor = monitor;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public User getcUser() {
        return cUser;
    }

    public void setcUser(User cUser) {
        this.cUser = cUser;
    }

    public User getmUser() {
        return mUser;
    }

    public void setmUser(User mUser) {
        this.mUser = mUser;
    }

    @Override
    public String toString() {
        return "ConfiguredChair{" +
                "id=" + getId() +
                ", movieSchedule=" + movieSchedule.getId() +
                ", customer=" + customer.getUser().getUsername() +
                ", isBooked=" + isBooked +
                '}';
    }
}
