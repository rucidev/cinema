package com.ikubinfo.cinema.model.customer;

import com.ikubinfo.cinema.commons.entity.EntityFilter;

public class CustomerFilter extends EntityFilter {
    private boolean isFullBody = true;
    private int userID = MINUS_ONE;

    public CustomerFilter(){ }

    public CustomerFilter(boolean isFullBody){
        this.isFullBody = isFullBody;
    }

    public CustomerFilter(int id, boolean isFullBody){
        super(id);
        this.isFullBody = isFullBody;
    }

    public boolean isFullBody() {
        return isFullBody;
    }

    public void setFullBody(boolean fullBody) {
        isFullBody = fullBody;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }
}
