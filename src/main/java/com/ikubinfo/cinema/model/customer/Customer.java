package com.ikubinfo.cinema.model.customer;

import com.ikubinfo.cinema.commons.entity.Entity;
import com.ikubinfo.cinema.model.user.User;

public class Customer extends Entity {
    private User user;
    private int ticketsBought;

    public Customer(){ }

    public Customer(int id){
        super(id);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getTicketsBought() {
        return ticketsBought;
    }

    public void setTicketsBought(int ticketsBought) {
        this.ticketsBought = ticketsBought;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "user=" + user +
                ", ticketsBought=" + ticketsBought +
                '}';
    }
}
