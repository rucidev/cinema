package com.ikubinfo.cinema.commons.db.jdbc;

public class CinemaResourceConfig {
    public static final String DB_REMOTE = "sql7235211";
    public static final String DB_LOCAL = "cinema";
    public static final boolean TEST_MODE = false;

    public static DBConnectionConfig getDBConnectionConfig() {
        DBConnectionConfig dbConnectionConfig = new DBConnectionConfig();
        String driver = null;
        String url = null;
        String username = null;
        String password = null;
        String host = null;
        String dbName = null;

        if (TEST_MODE) {
            System.out.println("Connecting to local database...");

            //Credentials for local db
            driver = "com.mysql.jdbc.Driver";
            url = "jdbc:mysql://localhost:3306/cinema?useSSL=false";
            username = "root";
            password = "root";
            host = "127.0.0.1:3306";
            dbName = "cinema";
        } else {
            System.out.println("Connecting to FreeMySqlHosting database...");

            //Credentials for remote db
            driver = "com.mysql.jdbc.Driver";
            url = "jdbc:mysql://sql7.freemysqlhosting.net/" + getDbName();
            username = "sql7235211";
            password = "9pVzT5viNq";
            host = "jdbc:mysql://sql7.freemysqlhosting.net";
            dbName = getDbName();
        }

        dbConnectionConfig.setDriverClassName(driver);
        dbConnectionConfig.setURL(url);
        dbConnectionConfig.setUserName(username);
        dbConnectionConfig.setPassword(password);
        dbConnectionConfig.setHost(host);
        dbConnectionConfig.setDbName(dbName);

        return dbConnectionConfig;
    }

    public static String getDbName() {
        return TEST_MODE ? DB_LOCAL : DB_REMOTE;
    }
}
