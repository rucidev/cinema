package com.ikubinfo.cinema.commons.db.jdbc;

import java.io.Serializable;

public class DBConnectionConfig implements Serializable {
    private static final long serialVersionUID = 1L;

    private String DriverClassName = "";
    private String URL = "";
    private String UserName = "";
    private String Password = "";
    private String dbName = "";
    private String host = "";

    public DBConnectionConfig() {
    }

    public String getDriverClassName() {
        return this.DriverClassName;
    }

    public void setDriverClassName(String driverClassName) {
        this.DriverClassName = driverClassName;
    }

    public String getURL() {
        return this.URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getUserName() {
        return this.UserName;
    }

    public void setUserName(String userName) {
        this.UserName = userName;
    }

    public String getPassword() {
        return this.Password;
    }

    public void setPassword(String password) {
        this.Password = password;
    }

    public String getDbName() {
        return this.dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public String getHost() {
        return this.host;
    }

    public void setHost(String host) {
        this.host = host;
    }
}

