package com.ikubinfo.cinema.commons.db.hibernate;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Created by Aleks on Jun 09, 2018
 */
public class HibernateConnection {
    public static int MINUS_ONE = -1;
    private static final String defaultPersistenceUnitName = "cinemaremote";

    private EntityManagerFactory entityManagerFactory = null;
    private EntityManager entityManager = null;

    public HibernateConnection() {
    }

    public HibernateConnection(HibernateConnection parentAdapter) throws Exception {
        this.entityManager = parentAdapter.getEntityManager();
    }

    public EntityManager getNewEntityManager(String persistenceUnitName) {
        try {
            entityManagerFactory = Persistence.createEntityManagerFactory(defaultPersistenceUnitName);
            entityManager = entityManagerFactory.createEntityManager();
            entityManager.getTransaction().begin();
        }catch (Exception e){
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return entityManager;
    }

    public EntityManager getNewEntityManager(){
        return getNewEntityManager(defaultPersistenceUnitName);
    }

    public EntityManager getEntityManager(){
        try {
            if (this.getEm() == null || !this.getEm().isOpen()) {
                this.setEm(getNewEntityManager());
            }
            return this.getEm();
        }catch (Exception e){
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return this.getEm();
    }

    public void setEm(EntityManager em){
        this.entityManager = em;
    }

    public EntityManager getEm(){
        return this.entityManager;
    }

    public EntityManagerFactory getEmf(){
        return this.entityManagerFactory;
    }
}
