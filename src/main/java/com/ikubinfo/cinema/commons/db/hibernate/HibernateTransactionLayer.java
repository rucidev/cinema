package com.ikubinfo.cinema.commons.db.hibernate;

/**
 * Created by Aleks on Jun 09, 2018
 */
public abstract class HibernateTransactionLayer{
    public DefaultHibernateLayer hibernateLayer = null;
    HibernateTransactionLayer parentTransactionLayer = null;

    public HibernateTransactionLayer() {
        this.hibernateLayer = this.createHibernateAdapter();
    }

    public HibernateTransactionLayer(HibernateTransactionLayer parentTransactionLayer) throws Exception {
        this.hibernateLayer = this.createHibernateAdapter(parentTransactionLayer.getHibernateLayer());
        this.parentTransactionLayer = parentTransactionLayer;
    }

    public abstract DefaultHibernateLayer createHibernateAdapter();

    public abstract DefaultHibernateLayer createHibernateAdapter(DefaultHibernateLayer var1) throws Exception;

    public DefaultHibernateLayer getHibernateLayer() {
        if (this.hibernateLayer == null) {
            this.hibernateLayer = this.createHibernateAdapter();
        }

        return this.hibernateLayer;
    }

    public void attemptToCommit() throws Exception {
        if (!this.hasParentTransaction()) {
            this.getHibernateLayer().attemptToCommit();
        }

    }

    public void attemptToRollBack(Exception e) throws Exception {
        this.attemptToRollBack();
        throw e;
    }

    public void attemptToRollBack() throws Exception {
        if (!this.hasParentTransaction()) {
            this.getHibernateLayer().rollBack();
        }

    }

    public void closeHibernateConnection() {
        if (!this.hasParentTransaction()) {
            this.getHibernateLayer().closeHibernateConnection();
        }

    }

    public boolean hasParentTransaction() {
        return this.parentTransactionLayer != null;
    }
}
