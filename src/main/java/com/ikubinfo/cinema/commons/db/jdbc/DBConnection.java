package com.ikubinfo.cinema.commons.db.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBConnection {
    public static int MINUS_ONE = -1;

    private DBConnectionConfig dbConnectionConfig = null;
    private Connection con = null;

    public DBConnection() {
    }

    public DBConnection(DBConnection parentAdapter) throws Exception {
        this.con = parentAdapter.getConnection();
        this.getConnection().setAutoCommit(false);
    }

    public Connection getNewConnection(){
        try{
            dbConnectionConfig = CinemaResourceConfig.getDBConnectionConfig();
            Class.forName(dbConnectionConfig.getDriverClassName());
            con = DriverManager.getConnection(dbConnectionConfig.getURL(), dbConnectionConfig.getUserName(), dbConnectionConfig.getPassword());
        } catch (Exception e){
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return con;
    }

    public Connection getConnection() throws Exception {
        if (this.getCon() == null || this.getCon().isClosed()) {
            this.setCon(getNewConnection());
            this.getCon().setAutoCommit(false);
        }

        return this.getCon();
    }

    public Connection getCon() {
        return con;
    }

    public void setCon(Connection con) {
        this.con = con;
    }
}
