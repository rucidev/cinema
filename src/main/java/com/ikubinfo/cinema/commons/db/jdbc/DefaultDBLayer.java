package com.ikubinfo.cinema.commons.db.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class DefaultDBLayer extends DBConnection {
    public DefaultDBLayer() {
    }

    public DefaultDBLayer(DefaultDBLayer parentDbLayer) throws Exception {
        super(parentDbLayer);
    }

    public PreparedStatement getPStmtWithGeneratedID(String query) throws Exception {
        printQuery(query);
        return this.getConnection().prepareStatement(query, 1);
    }

    public void printQuery(String query) {
        System.out.println(query + "\n");
    }

    public int getGeneratedID(PreparedStatement stmt) throws Exception {
        ResultSet keyResultSet = stmt.getGeneratedKeys();
        if (keyResultSet.next()) {
            return keyResultSet.getInt(1);
        } else {
            throw new Exception("Next ID has not been generated!");
        }
    }

    public void closeDBConnection() {
        try {
            if (this.getCon() != null && !this.getCon().isClosed()) {
                this.getCon().close();
            }
        } catch (Exception var2) {
            var2.printStackTrace();
        }

        this.setCon((Connection)null);
    }

    public final void attemptToCommit() throws Exception {
        this.getConnection().commit();
    }

    public void rollBack() {
        try {
            this.getConnection().rollback();
        } catch (Exception var2) {
            var2.printStackTrace();
        }
    }
}
