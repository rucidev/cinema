package com.ikubinfo.cinema.commons.db.jdbc;

public abstract class TransactionLayer {
    public DefaultDBLayer dbLayer = null;
    TransactionLayer parentTransactionLayer = null;

    public TransactionLayer() {
        this.dbLayer = this.createDBAdapter();
    }

    public TransactionLayer(TransactionLayer parentTransactionLayer) throws Exception {
        this.dbLayer = this.createDBAdapter(parentTransactionLayer.getDBLayer());
        this.parentTransactionLayer = parentTransactionLayer;
    }

    public abstract DefaultDBLayer createDBAdapter();

    public abstract DefaultDBLayer createDBAdapter(DefaultDBLayer var1) throws Exception;

    public DefaultDBLayer getDBLayer() {
        if (this.dbLayer == null) {
            this.dbLayer = this.createDBAdapter();
        }

        return this.dbLayer;
    }

    public void attemptToCommit() throws Exception {
        if (!this.hasParentTransaction()) {
            this.getDBLayer().attemptToCommit();
        }

    }

    public void attemptToRollBack(Exception e) throws Exception {
        this.attemptToRollBack();
        throw e;
    }

    public void attemptToRollBack() throws Exception {
        if (!this.hasParentTransaction()) {
            this.getDBLayer().rollBack();
        }

    }

    public void closeDBConnection() {
        if (!this.hasParentTransaction()) {
            this.getDBLayer().closeDBConnection();
        }

    }

    public boolean hasParentTransaction() {
        return this.parentTransactionLayer != null;
    }
}
