package com.ikubinfo.cinema.commons.db.hibernate;

import com.ikubinfo.cinema.commons.db.jdbc.DefaultDBLayer;
import org.hibernate.Hibernate;

import javax.persistence.EntityManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created by Aleks on Jun 09, 2018
 */
public class DefaultHibernateLayer extends HibernateConnection {

    public DefaultHibernateLayer() {
    }

    public DefaultHibernateLayer(DefaultHibernateLayer parentDbLayer) throws Exception {
        super(parentDbLayer);
    }

    public void printQuery(String query) {
        System.out.println(query + "\n");
    }

    public void closeHibernateConnection() {
        try {
            if (this.getEm() != null && this.getEm().isOpen()) {
                this.getEm().close();
                this.getEmf().close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        this.setEm((EntityManager) null);
    }

    public final void attemptToCommit() throws Exception {
        this.getEm().getTransaction().commit();
    }

    public void rollBack() {
        try {
            this.getEm().getTransaction().rollback();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
