package com.ikubinfo.cinema.commons.date;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ASUtilTime extends Date {
    public static final SimpleDateFormat formatTimeMySql = new SimpleDateFormat("HH:mm:ss");

    public ASUtilTime() {
    }

    public ASUtilTime(Date time) {
        super(time.getTime());
    }

    public ASUtilTime(String timeAsString) throws Exception {
        if (timeAsString != null && !timeAsString.equals("")) {
            if (timeAsString.length() == 5) {
                this.setTime((new SimpleDateFormat("HH:mm")).parse(timeAsString).getTime());
            } else if (timeAsString.length() == 8) {
                this.setTime((new SimpleDateFormat("HH:mm:ss")).parse(timeAsString).getTime());
            } else if (timeAsString.length() == 9) {
                this.setTime((new SimpleDateFormat("HH:mm:sss")).parse(timeAsString).getTime());
            } else {
                throw new Exception("Unparsable Date: " + timeAsString);
            }

        } else {
            throw new Exception("Not valid time");
        }
    }

    public String toMySqlTime() {
        return (new SimpleDateFormat("HH:mm:ss")).format(this.getTime());
    }
}
