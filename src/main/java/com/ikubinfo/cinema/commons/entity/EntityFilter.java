package com.ikubinfo.cinema.commons.entity;

public class EntityFilter {
    public static int MINUS_ONE = -1;

    private int id = MINUS_ONE;
    private int[] entitiesID = null;
    private String description = "";

    public EntityFilter(){ }

    public EntityFilter(int id){
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int[] getEntitiesID() {
        return entitiesID;
    }

    public void setEntitiesID(int[] entitiesID) {
        this.entitiesID = entitiesID;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
