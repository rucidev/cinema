package com.ikubinfo.cinema.commons.entity;

import com.ikubinfo.cinema.commons.date.ASUtilDate;

import java.io.Serializable;

public class Entity implements Serializable {
    public static final int MINUS_ONE = -1;

    private int id = MINUS_ONE;
    private String description = "";
    private ASUtilDate cDate;
    private ASUtilDate mDate;


    public Entity() {
    }

    public Entity(int id) {
        this.setId(id);
    }

    public Entity(String description) {
        this.setDescription(description);
    }

    public Entity(int id, String description) {
        this(id);
        this.setDescription(description);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ASUtilDate getcDate() {
        return cDate;
    }

    public void setcDate(ASUtilDate cDate) {
        this.cDate = cDate;
    }

    public ASUtilDate getmDate() {
        return mDate;
    }

    public void setmDate(ASUtilDate mDate) {
        this.mDate = mDate;
    }

    @Override
    public String toString() {
        return "Entity{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", cDate=" + cDate +
                ", mDate=" + mDate +
                '}';
    }
}
