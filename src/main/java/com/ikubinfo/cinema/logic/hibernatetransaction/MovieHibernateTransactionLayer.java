package com.ikubinfo.cinema.logic.hibernatetransaction;

import com.ikubinfo.cinema.commons.db.hibernate.DefaultHibernateLayer;
import com.ikubinfo.cinema.commons.db.hibernate.HibernateTransactionLayer;
import com.ikubinfo.cinema.db.hibernate.MovieDAOHibernate;
import com.ikubinfo.cinema.model.movie.Movie;
import com.ikubinfo.cinema.model.movie.Movie1;
import com.ikubinfo.cinema.model.movie.MovieFilter;

import java.util.List;

/**
 * Created by Aleks on Jun 09, 2018
 */
public class MovieHibernateTransactionLayer extends HibernateTransactionLayer {
    public MovieHibernateTransactionLayer() {
        super();
    }

    public MovieHibernateTransactionLayer(HibernateTransactionLayer parentTransactionLayer) throws Exception {
        super(parentTransactionLayer);
    }

    @Override
    public MovieDAOHibernate createHibernateAdapter() {
        hibernateLayer = new MovieDAOHibernate();
        return (MovieDAOHibernate) hibernateLayer;
    }

    @Override
    public DefaultHibernateLayer createHibernateAdapter(DefaultHibernateLayer parentDbLayer) throws Exception {
        hibernateLayer = new MovieDAOHibernate(parentDbLayer);
        return hibernateLayer;
    }

    @Override
    public MovieDAOHibernate getHibernateLayer() {
        if (hibernateLayer == null) {
            hibernateLayer = createHibernateAdapter();
        }
        return (MovieDAOHibernate) hibernateLayer;
    }



    public void saveMovie1(Movie1 movie1) throws Exception {
        try {
            getHibernateLayer().saveMovie1(movie1);
            attemptToCommit();
        } catch (Exception e) {
            attemptToRollBack(e);
        } finally {
            closeHibernateConnection();
        }
    }


    public void updateMovie(Movie movie) throws Exception {
        try {
            getHibernateLayer().updateMovie(movie);
            attemptToCommit();
        } catch (Exception e) {
            attemptToRollBack(e);
        } finally {
            closeHibernateConnection();
        }
    }

    public void deleteMovie(int id) throws Exception {
        try {
            getHibernateLayer().deleteMovie(id);
            attemptToCommit();
        } catch (Exception e) {
            attemptToRollBack(e);
        } finally {
            closeHibernateConnection();
        }
    }

    public List<Movie> getMovie(MovieFilter movieFilter) throws Exception {
        try {
            return getHibernateLayer().getMovies(movieFilter);
        } finally {
            closeHibernateConnection();
        }
    }
}
