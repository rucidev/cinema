package com.ikubinfo.cinema.logic.jdbctransaction;

import com.ikubinfo.cinema.commons.db.jdbc.DefaultDBLayer;
import com.ikubinfo.cinema.commons.db.jdbc.TransactionLayer;
import com.ikubinfo.cinema.db.jdbc.ConfiguredChairDAOImpl;
import com.ikubinfo.cinema.model.chair.configuredchair.ConfiguredChair;
import com.ikubinfo.cinema.model.chair.configuredchair.ConfiguredChairFilter;

import java.util.List;

/**
 * Created by Aleks on May 29, 2018
 */
public class ConfiguredChairTransactionLayer extends TransactionLayer {
    public ConfiguredChairTransactionLayer() {
        super();
    }

    public ConfiguredChairTransactionLayer(TransactionLayer parentTransactionLayer) throws Exception {
        super(parentTransactionLayer);
    }

    @Override
    public ConfiguredChairDAOImpl createDBAdapter() {
        dbLayer = new ConfiguredChairDAOImpl();
        return (ConfiguredChairDAOImpl) dbLayer;
    }

    @Override
    public DefaultDBLayer createDBAdapter(DefaultDBLayer parentDbLayer) throws Exception {
        dbLayer = new ConfiguredChairDAOImpl(parentDbLayer);
        return dbLayer;
    }

    @Override
    public ConfiguredChairDAOImpl getDBLayer() {
        if (dbLayer == null) {
            dbLayer = createDBAdapter();
        }
        return (ConfiguredChairDAOImpl) dbLayer;
    }

    public void updateConfiguredChair(ConfiguredChair configuredChair) throws Exception{
        try {
            getDBLayer().updateConfiguredChair(configuredChair);
            attemptToCommit();
        } catch (Exception e) {
            attemptToRollBack(e);
        } finally {
            closeDBConnection();
        }
    }

    public List<ConfiguredChair> getConfiguredChairs(ConfiguredChairFilter configuredChairFilter) throws Exception {
        try {
            return getDBLayer().getConfiguredChairs(configuredChairFilter);
        } finally {
            closeDBConnection();
        }
    }
}
