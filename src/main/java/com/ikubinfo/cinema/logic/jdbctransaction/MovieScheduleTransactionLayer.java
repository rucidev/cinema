package com.ikubinfo.cinema.logic.jdbctransaction;

import com.ikubinfo.cinema.commons.db.jdbc.DefaultDBLayer;
import com.ikubinfo.cinema.commons.db.jdbc.TransactionLayer;
import com.ikubinfo.cinema.db.jdbc.MovieScheduleDAOImpl;
import com.ikubinfo.cinema.model.movie.movieschedule.MovieSchedule;
import com.ikubinfo.cinema.model.movie.movieschedule.MovieScheduleFilter;

import java.util.List;

/**
 * Created by Aleks on May 29, 2018
 */
public class MovieScheduleTransactionLayer extends TransactionLayer {

    public MovieScheduleTransactionLayer() {
        super();
    }

    public MovieScheduleTransactionLayer(TransactionLayer parentTransactionLayer) throws Exception {
        super(parentTransactionLayer);
    }

    @Override
    public MovieScheduleDAOImpl createDBAdapter() {
        dbLayer = new MovieScheduleDAOImpl();
        return (MovieScheduleDAOImpl) dbLayer;
    }

    @Override
    public DefaultDBLayer createDBAdapter(DefaultDBLayer parentDbLayer) throws Exception {
        dbLayer = new MovieScheduleDAOImpl(parentDbLayer);
        return dbLayer;
    }

    @Override
    public MovieScheduleDAOImpl getDBLayer() {
        if (dbLayer == null) {
            dbLayer = createDBAdapter();
        }
        return (MovieScheduleDAOImpl) dbLayer;
    }

    public void saveMovieSchedule(MovieSchedule movieSchedule) throws Exception {
        try {
            getDBLayer().saveMovieSchedule(movieSchedule);
            attemptToCommit();
        } catch (Exception e) {
            attemptToRollBack(e);
        } finally {
            closeDBConnection();
        }
    }

    public void updateMovieSchedule(MovieSchedule movieSchedule) throws Exception {
        try {
            getDBLayer().updateMovieSchedule(movieSchedule);
            attemptToCommit();
        } catch (Exception e) {
            attemptToRollBack(e);
        } finally {
            closeDBConnection();
        }
    }

    public void deleteMovieSchedule(Integer id) throws Exception {
        try {
            getDBLayer().deleteMovieSchedule(id);
            attemptToCommit();
        } catch (Exception e) {
            attemptToRollBack(e);
        } finally {
            closeDBConnection();
        }
    }

    public List<MovieSchedule> getMovieSchedules(MovieScheduleFilter movieScheduleFilter) throws Exception{
        try {
            return getDBLayer().getMovieSchedules(movieScheduleFilter);
        } finally {
            closeDBConnection();
        }
    }
}
