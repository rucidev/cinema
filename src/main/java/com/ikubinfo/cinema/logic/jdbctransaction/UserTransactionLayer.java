package com.ikubinfo.cinema.logic.jdbctransaction;

import com.ikubinfo.cinema.commons.db.jdbc.DefaultDBLayer;
import com.ikubinfo.cinema.commons.db.jdbc.TransactionLayer;
import com.ikubinfo.cinema.db.jdbc.UserDAOImpl;
import com.ikubinfo.cinema.model.administrator.Administrator;
import com.ikubinfo.cinema.model.administrator.AdministratorFilter;
import com.ikubinfo.cinema.model.customer.Customer;
import com.ikubinfo.cinema.model.customer.CustomerFilter;
import com.ikubinfo.cinema.model.reservation.Reservation;
import com.ikubinfo.cinema.model.user.User;
import com.ikubinfo.cinema.model.user.UserFilter;

import java.util.ArrayList;

public class UserTransactionLayer extends TransactionLayer {
    public UserTransactionLayer() {
        super();
    }

    public UserTransactionLayer(TransactionLayer parentTransactionLayer) throws Exception {
        super(parentTransactionLayer);
    }

    @Override
    public UserDAOImpl createDBAdapter() {
        dbLayer = new UserDAOImpl();
        return (UserDAOImpl) dbLayer;
    }

    @Override
    public DefaultDBLayer createDBAdapter(DefaultDBLayer parentDbLayer) throws Exception {
        dbLayer = new UserDAOImpl(parentDbLayer);
        return dbLayer;
    }

    @Override
    public UserDAOImpl getDBLayer() {
        if (dbLayer == null) {
            dbLayer = createDBAdapter();
        }
        return (UserDAOImpl) dbLayer;
    }

    // TODO: 4/29/2018 update, delete user
    public void saveUser(User user) throws Exception {
        try {
            getDBLayer().saveUser(user);
            attemptToCommit();
        } catch (Exception e) {
            attemptToRollBack(e);
        } finally {
            closeDBConnection();
        }
    }
//
//    public void updateUser(com.cinema.User user) throws Exception {
//        try {
//            getHibernateLayer().updateUser(user);
//            attemptToCommit();
//        } catch (Exception e) {
//            attemptToRollBack(e);
//        } finally {
//            closeDBConnection();
//        }
//    }
//
//    public void deleteUser(Integer id) throws Exception {
//        try {
//            getHibernateLayer().removeUser(id);
//            attemptToCommit();
//        } catch (Exception e) {
//            attemptToRollBack(e);
//        } finally {
//            closeDBConnection();
//        }
//    }

    public ArrayList<User> getUsers(UserFilter userFilter) throws Exception {
        try {
            return getDBLayer().getUsers(userFilter);
        } finally {
            closeDBConnection();
        }
    }

    //Start administrator

    // TODO: 4/29/2018 do save, update, delete administrator
//    public void saveMonitor(Administrator administrator) throws Exception {
//        try {
//            getHibernateLayer().saveAdministrator(administrator);
//            attemptToCommit();
//        } catch (Exception e) {
//            attemptToRollBack(e);
//        } finally {
//            closeDBConnection();
//        }
//    }
//
//    public void updateMonitor(Administrator administrator) throws Exception {
//        try {
//            getHibernateLayer().updateAdministrator(administrator);
//            attemptToCommit();
//        } catch (Exception e) {
//            attemptToRollBack(e);
//        } finally {
//            closeDBConnection();
//        }
//    }
//
//    public void deleteMonitor(Integer id) throws Exception {
//        try {
//            getHibernateLayer().removeAdministrator(id);
//            attemptToCommit();
//        } catch (Exception e) {
//            attemptToRollBack(e);
//        } finally {
//            closeDBConnection();
//        }
//    }

    public ArrayList<Administrator> getAdministrators(AdministratorFilter administratorFilter) throws Exception {
        try {
            return getDBLayer().getAdministrators(administratorFilter);
        } finally {
            closeDBConnection();
        }
    }

    //End adminsitrator


    //Start customer
    public void saveCustomer(Customer customer) throws Exception {
        try {
            getDBLayer().saveCustomer(customer);
            attemptToCommit();
        } catch (Exception e) {
            attemptToRollBack(e);
        } finally {
            closeDBConnection();
        }
    }

    public ArrayList<Customer> getCustomers(CustomerFilter customerFilter) throws Exception {
        try {
            return getDBLayer().getCustomers(customerFilter);
        } finally {
            closeDBConnection();
        }
    }

    //End customer

    //Start reservation
    public void saveReservation(Reservation reservation) throws Exception {
        try {
            getDBLayer().saveReservation(reservation);
            attemptToCommit();
        } catch (Exception e) {
            attemptToRollBack(e);
        } finally {
            closeDBConnection();
        }
    }

}
