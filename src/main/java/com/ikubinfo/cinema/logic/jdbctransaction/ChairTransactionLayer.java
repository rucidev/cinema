package com.ikubinfo.cinema.logic.jdbctransaction;

import com.ikubinfo.cinema.commons.db.jdbc.DefaultDBLayer;
import com.ikubinfo.cinema.commons.db.jdbc.TransactionLayer;
import com.ikubinfo.cinema.db.jdbc.ChairDAOImpl;

public class ChairTransactionLayer extends TransactionLayer {
    public ChairTransactionLayer() {
        super();
    }

    public ChairTransactionLayer(TransactionLayer parentTransactionLayer) throws Exception {
        super(parentTransactionLayer);
    }

    @Override
    public ChairDAOImpl createDBAdapter() {
        dbLayer = new ChairDAOImpl();
        return (ChairDAOImpl) dbLayer;
    }

    @Override
    public DefaultDBLayer createDBAdapter(DefaultDBLayer parentDbLayer) throws Exception {
        dbLayer = new ChairDAOImpl(parentDbLayer);
        return dbLayer;
    }

    @Override
    public ChairDAOImpl getDBLayer() {
        if (dbLayer == null) {
            dbLayer = createDBAdapter();
        }
        return (ChairDAOImpl) dbLayer;
    }

}
