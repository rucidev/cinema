package com.ikubinfo.cinema.logic.jdbctransaction;

import com.ikubinfo.cinema.commons.db.jdbc.DefaultDBLayer;
import com.ikubinfo.cinema.commons.db.jdbc.TransactionLayer;
import com.ikubinfo.cinema.db.jdbc.MonitorDAOImpl;
import com.ikubinfo.cinema.model.monitor.Monitor;
import com.ikubinfo.cinema.model.monitor.MonitorFilter;

import java.util.List;

public class MonitorTransactionLayer extends TransactionLayer {
    public MonitorTransactionLayer() {
        super();
    }

    public MonitorTransactionLayer(TransactionLayer parentTransactionLayer) throws Exception {
        super(parentTransactionLayer);
    }

    @Override
    public MonitorDAOImpl createDBAdapter() {
        dbLayer = new MonitorDAOImpl();
        return (MonitorDAOImpl) dbLayer;
    }

    @Override
    public DefaultDBLayer createDBAdapter(DefaultDBLayer parentDbLayer) throws Exception {
        dbLayer = new MonitorDAOImpl(parentDbLayer);
        return dbLayer;
    }

    @Override
    public MonitorDAOImpl getDBLayer() {
        if (dbLayer == null) {
            dbLayer = createDBAdapter();
        }
        return (MonitorDAOImpl) dbLayer;
    }

    public void saveMonitor(Monitor monitor) throws Exception {
        try {
            getDBLayer().saveMonitor(monitor);
            attemptToCommit();
        } catch (Exception e) {
            attemptToRollBack(e);
        } finally {
            closeDBConnection();
        }
    }

    public void updateMonitor(Monitor monitor) throws Exception {
        try {
            getDBLayer().updateMonitor(monitor);
            attemptToCommit();
        } catch (Exception e) {
            attemptToRollBack(e);
        } finally {
            closeDBConnection();
        }
    }

    public void deleteMonitor(Integer id) throws Exception {
        try {
            getDBLayer().deleteMonitor(id);
            attemptToCommit();
        } catch (Exception e) {
            attemptToRollBack(e);
        } finally {
            closeDBConnection();
        }
    }

    public List<Monitor> getMonitors(MonitorFilter monitorFilter) throws Exception {
        try {
            return getDBLayer().getMonitors(monitorFilter);
        } finally {
            closeDBConnection();
        }
    }
}
