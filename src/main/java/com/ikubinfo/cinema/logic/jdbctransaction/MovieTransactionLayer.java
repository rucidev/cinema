package com.ikubinfo.cinema.logic.jdbctransaction;

import com.ikubinfo.cinema.commons.db.jdbc.DefaultDBLayer;
import com.ikubinfo.cinema.commons.db.jdbc.TransactionLayer;
import com.ikubinfo.cinema.db.jdbc.MovieDAOImpl;
import com.ikubinfo.cinema.model.movie.Movie;
import com.ikubinfo.cinema.model.movie.MovieFilter;

import java.util.List;

public class MovieTransactionLayer extends TransactionLayer {

    public MovieTransactionLayer() {
        super();
    }

    public MovieTransactionLayer(TransactionLayer parentTransactionLayer) throws Exception {
        super(parentTransactionLayer);
    }

    @Override
    public MovieDAOImpl createDBAdapter() {
        dbLayer = new MovieDAOImpl();
        return (MovieDAOImpl) dbLayer;
    }

    @Override
    public DefaultDBLayer createDBAdapter(DefaultDBLayer parentDbLayer) throws Exception {
        dbLayer = new MovieDAOImpl(parentDbLayer);
        return dbLayer;
    }

    @Override
    public MovieDAOImpl getDBLayer() {
        if (dbLayer == null) {
            dbLayer = createDBAdapter();
        }
        return (MovieDAOImpl) dbLayer;
    }

    public void saveMovie(Movie movie) throws Exception {
        try {
            getDBLayer().saveMovie(movie);
            attemptToCommit();
        } catch (Exception e) {
            attemptToRollBack(e);
        } finally {
            closeDBConnection();
        }
    }

    public void updateMovie(Movie movie) throws Exception {
        try {
            getDBLayer().updateMovie(movie);
            attemptToCommit();
        } catch (Exception e) {
            attemptToRollBack(e);
        } finally {
            closeDBConnection();
        }
    }

    public void deleteMovie(Integer id) throws Exception {
        try {
            getDBLayer().deleteMovie(id);
            attemptToCommit();
        } catch (Exception e) {
            attemptToRollBack(e);
        } finally {
            closeDBConnection();
        }
    }

    public List<Movie> getMovies(MovieFilter movieFilter) throws Exception {
        try {
            return getDBLayer().getMovies(movieFilter);
        } finally {
            closeDBConnection();
        }
    }

}
