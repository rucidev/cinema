package com.ikubinfo.cinema.logic.manager;

import com.ikubinfo.cinema.algorithm.MovieAlgorithm;
import com.ikubinfo.cinema.db.jdbc.ConfiguredChairDAOImpl;
import com.ikubinfo.cinema.logic.jdbctransaction.*;
import com.ikubinfo.cinema.model.administrator.Administrator;
import com.ikubinfo.cinema.model.administrator.AdministratorFilter;
import com.ikubinfo.cinema.model.chair.configuredchair.ConfiguredChair;
import com.ikubinfo.cinema.model.chair.configuredchair.ConfiguredChairFilter;
import com.ikubinfo.cinema.model.customer.Customer;
import com.ikubinfo.cinema.model.customer.CustomerFilter;
import com.ikubinfo.cinema.model.monitor.Monitor;
import com.ikubinfo.cinema.model.monitor.MonitorFilter;
import com.ikubinfo.cinema.model.movie.EMovieType;
import com.ikubinfo.cinema.model.movie.Movie;
import com.ikubinfo.cinema.model.movie.MovieFilter;
import com.ikubinfo.cinema.model.movie.movieschedule.MovieSchedule;
import com.ikubinfo.cinema.model.movie.movieschedule.MovieScheduleFilter;
import com.ikubinfo.cinema.model.reservation.Reservation;
import com.ikubinfo.cinema.model.user.User;
import com.ikubinfo.cinema.model.user.UserFilter;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class SystemManager {
    private static final Logger log = Logger.getLogger(SystemManager.class.getName());

    //Start Monitor
    public void addMonitor(Monitor monitor) {
        try {
            new MonitorTransactionLayer().saveMonitor(monitor);
        } catch (Exception e) {
            log.info("SystemManager.addMonitor = " + e.getMessage());
        }
    }

    public void updateMonitor(Monitor monitor) {
        try {
            new MonitorTransactionLayer().updateMonitor(monitor);
        } catch (Exception e) {
            log.info("SystemManager.updateMonitor = " + e.getMessage());
        }
    }

    public void removeMonitor(int id) throws Exception {
        try {
            new MonitorTransactionLayer().deleteMonitor(id);
        } catch (Exception e) {
            log.info("SystemManager.updateMonitor = " + e.getMessage());
        }
    }

    public List<Monitor> getAllMonitors() throws Exception {
        return new MonitorTransactionLayer().getMonitors(new MonitorFilter(true));
    }

    public Monitor getMonitorByID(int id) throws Exception {
        List<Monitor> monitors = new MonitorTransactionLayer().getMonitors(new MonitorFilter(id, false));
        if (monitors.size() == 0) return null;
        return monitors.get(0);
    }

    public List<Monitor> getMonitorsByRoomID(int roomID) throws Exception {
        MonitorFilter monitorFilter = new MonitorFilter(false);
        monitorFilter.setRoomID(roomID);
        return new MonitorTransactionLayer().getMonitors(monitorFilter);
    }
    //End Monitor


    // Start Movie
    public void addMovie(Movie movie) throws Exception {
        new MovieTransactionLayer().saveMovie(movie);
    }

    public void updateMovie(Movie movie) throws Exception {
        new MovieTransactionLayer().updateMovie(movie);
    }

    public void removeMovie(int id) throws Exception {
        new MovieTransactionLayer().deleteMovie(id);
    }

    public List<Movie> getAllMovies() throws Exception {
        return new MovieTransactionLayer().getMovies(new MovieFilter(true));
    }

    public Movie getMovieByID(int id) throws Exception {
        List<Movie> movies = new MovieTransactionLayer().getMovies(new MovieFilter(id, false));
        if (movies.size() == 0) return null;
        return movies.get(0);
    }

    public List<Movie> getMoviesByMovieType(int movieTypeID) throws Exception {
        MovieFilter movieFilter = new MovieFilter(false);
        movieFilter.setMovieTypeID(movieTypeID);
        return new MovieTransactionLayer().getMovies(movieFilter);
    }

    public Movie getMovieByTitle(String title) throws Exception {
        List<Movie> movies = new MovieTransactionLayer().getMovies(new MovieFilter(title, false));
        if (movies.size() == 0) return null;
        return movies.get(0);
    }
    //End Movie


    //Start Administrator
    // TODO: 4/29/2018 save, remove, update Administrator
//    public void addAdministrator(Administrator administrator) throws Exception {
//        new AdministratorTransactionLayer().saveAdministrator(administrator);
//    }
//
//    public void updateAdministrator(Administrator administrator) throws Exception {
//        new AdministratorTransactionLayer().updateAdministrator(administrator);
//    }
//
//    public void removeAdministrator(int id) throws Exception {
//        new AdministratorTransactionLayer().deleteAdministrator(id);
//    }

    public List<Administrator> getAlAdministrators() throws Exception {
        return new UserTransactionLayer().getAdministrators(new AdministratorFilter(true));
    }

    public Administrator getAdministratorByID(int id) throws Exception {
        ArrayList<Administrator> administrators = new UserTransactionLayer().getAdministrators(new AdministratorFilter(id, false));
        if (administrators.size() == 0) return null;
        return administrators.get(0);
    }

    public Administrator getAdministratorByUserID(int userID) throws Exception {
        AdministratorFilter administratorFilter = new AdministratorFilter(false);
        administratorFilter.setUserID(userID);
        ArrayList<Administrator> administrators = new UserTransactionLayer().getAdministrators(administratorFilter);
        if (administrators.size() == 0) return null;
        return administrators.get(0);
    }
    //End Administrator

    //Start Customer
    public void addCustomer(Customer customer) throws Exception{
        new UserTransactionLayer().saveCustomer(customer);
    }

    public Customer getCustomerByUserID(int userID) throws Exception {
        CustomerFilter customerFilter = new CustomerFilter(false);
        customerFilter.setUserID(userID);
        ArrayList<Customer> customers = new UserTransactionLayer().getCustomers(customerFilter);
        if (customers.size() == 0) return null;
        return customers.get(0);
    }
    //End Customer


    //Start com.cinema.User
    // TODO: 4/29/2018 add, remove, update user
    public void addUser(User user) throws Exception{
        new UserTransactionLayer().saveUser(user);
    }

    public List<User> getAllUsers() throws Exception {
        return new UserTransactionLayer().getUsers(new UserFilter(true));
    }

    public User getUserByID(int id) throws Exception {
        ArrayList<User> users = new UserTransactionLayer().getUsers(new UserFilter(id, false));
        if (users.size() == 0) return null;
        return users.get(0);
    }

    public User getUserByUsername(String username) throws Exception {
        ArrayList<User> users = new UserTransactionLayer().getUsers(new UserFilter(username, false));
        return users.get(0);
    }
    //End com.cinema.User


    //Start MovieSchedule

    public void addMovieTimetable(MovieSchedule movieSchedule) throws Exception {
        // the time overlapping check can be done using triggers in DB
        // i did it in my local db, but since the online one did
        // not give me the privilege to implement the trigger
        // i am implementing the logic here
        // This is the trigger:
            /*
            DELIMITER $$
            CREATE TRIGGER movietimeoverlap
            BEFORE INSERT ON movietimetable
            FOR EACH ROW
                    BEGIN
            IF NEW.MonitorID = MonitorID AND NEW.EndTime >= StartTime AND EndTime >= NEW.StartTime
                    THEN
            SIGNAL SQLSTATE '02000' SET MESSAGE_TEXT = 'Warning: there is overlap in movie timetables! INSERT statement aborted!';
            END IF;
            END$$
                    DELIMITER ;
            */

        MovieAlgorithm.checkMovieScheduleOverlap(movieSchedule);
        new MovieScheduleTransactionLayer().saveMovieSchedule(movieSchedule);
    }

    public void updateMovieTimetable(MovieSchedule movieSchedule) throws Exception {
        MovieAlgorithm.checkMovieScheduleOverlap(movieSchedule);
        new MovieScheduleTransactionLayer().updateMovieSchedule(movieSchedule);

    }

    public void removeMovieTimetable(int id) throws Exception {
        new MovieScheduleTransactionLayer().deleteMovieSchedule(id);
    }

    public List<MovieSchedule> getAllMovieTimeTables() throws Exception {
        return new MovieScheduleTransactionLayer().getMovieSchedules(new MovieScheduleFilter(true));
    }

    //End MovieSchedule

    //Start MovieType

    public List<EMovieType> getAllMovieTypes() throws Exception{
        return EMovieType.getMovieTypes();
    }

    //End MovieType

    //Start ConfiguredChair
    public void updateConfiguredChair(ConfiguredChair configuredChair) throws Exception {
        new ConfiguredChairDAOImpl().updateConfiguredChair(configuredChair);
    }

    public List<ConfiguredChair> getAllConfiguredChairs() throws Exception{
        return new ConfiguredChairDAOImpl().getConfiguredChairs(new ConfiguredChairFilter(true));
    }

    //End ConfiguredChair

    //Start reservation

    public void addReservation(Reservation reservation) throws Exception{
        new UserTransactionLayer().saveReservation(reservation);
    }

    //End reservation
}
