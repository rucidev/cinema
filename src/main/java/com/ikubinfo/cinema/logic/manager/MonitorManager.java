package com.ikubinfo.cinema.logic.manager;

import com.ikubinfo.cinema.logic.jdbctransaction.MonitorTransactionLayer;
import com.ikubinfo.cinema.model.monitor.Monitor;
import com.ikubinfo.cinema.model.monitor.MonitorFilter;

import java.util.List;
import java.util.logging.Logger;

/**
 * Created by Aleks on May 28, 2018
 */
public class MonitorManager {
    private static final Logger log = Logger.getLogger(SystemManager.class.getName());

    public void addMonitor(Monitor monitor) {
        try {
            new MonitorTransactionLayer().saveMonitor(monitor);
        } catch (Exception e) {
            log.info("MonitorManger.addMonitor = " + e.getMessage());
        }
    }

    public void updateMonitor(Monitor monitor) {
        try {
            new MonitorTransactionLayer().updateMonitor(monitor);
        } catch (Exception e) {
            log.info("MonitorManger.updateMonitor = " + e.getMessage());
        }
    }

    public void removeMonitor(int id) throws Exception {
        try {
            new MonitorTransactionLayer().deleteMonitor(id);
        } catch (Exception e) {
            log.info("MonitorManger.updateMonitor = " + e.getMessage());
        }
    }

    public List<Monitor> getAllMonitors() throws Exception {
        return new MonitorTransactionLayer().getMonitors(new MonitorFilter(true));
    }

    public Monitor getMonitorByID(int id) throws Exception {
        List<Monitor> monitors = new MonitorTransactionLayer().getMonitors(new MonitorFilter(id, false));
        if (monitors.size() == 0) return null;
        return monitors.get(0);
    }

    public List<Monitor> getMonitorsByRoomID(int roomID) throws Exception {
        MonitorFilter monitorFilter = new MonitorFilter(false);
        monitorFilter.setRoomID(roomID);
        return new MonitorTransactionLayer().getMonitors(monitorFilter);
    }
}
