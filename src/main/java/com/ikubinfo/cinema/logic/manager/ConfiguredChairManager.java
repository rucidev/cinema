package com.ikubinfo.cinema.logic.manager;

import com.ikubinfo.cinema.logic.jdbctransaction.ConfiguredChairTransactionLayer;
import com.ikubinfo.cinema.model.chair.configuredchair.ConfiguredChair;
import com.ikubinfo.cinema.model.chair.configuredchair.ConfiguredChairFilter;

import java.util.List;

/**
 * Created by Aleks on May 28, 2018
 */
public class ConfiguredChairManager {
    public void updateConfiguredChair(ConfiguredChair configuredChair) throws Exception {
        new ConfiguredChairTransactionLayer().updateConfiguredChair(configuredChair);
    }

    public List<ConfiguredChair> getAllConfiguredChairs() throws Exception{
        return new ConfiguredChairTransactionLayer().getConfiguredChairs(new ConfiguredChairFilter(true));
    }
}
