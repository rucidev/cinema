package com.ikubinfo.cinema.db;

import com.ikubinfo.cinema.model.chair.Chair;
import com.ikubinfo.cinema.model.chair.configuredchair.ConfiguredChair;
import com.ikubinfo.cinema.model.chair.configuredchair.ConfiguredChairFilter;

import java.util.List;

/**
 * Created by Aleks on May 29, 2018
 */
public interface ConfiguredChairDAO {
    public void saveConfiguredChair(ConfiguredChair configuredChair) throws Exception;
    public void updateConfiguredChair(ConfiguredChair configuredChair) throws Exception;
    public void deleteConfiguredChair(int id) throws Exception;
    public List<ConfiguredChair> getConfiguredChairs(ConfiguredChairFilter filter) throws Exception;
}
