package com.ikubinfo.cinema.db;

import com.ikubinfo.cinema.model.chair.Chair;
import com.ikubinfo.cinema.model.chair.ChairFilter;

import java.util.List;

/**
 * Created by Aleks on May 29, 2018
 */
public interface ChairDAO {
    public void saveChair(Chair chair) throws Exception;
    public void updateChair(Chair chair) throws Exception;
    public void deleteChair(int id) throws Exception;
    public List<Chair> getChairs(ChairFilter chairFilter) throws Exception;
}
