package com.ikubinfo.cinema.db;

import com.ikubinfo.cinema.model.movie.Movie;
import com.ikubinfo.cinema.model.movie.MovieFilter;

import java.util.List;

/**
 * Created by Aleks on May 29, 2018
 */
public interface MovieDAO {
    public void saveMovie(Movie movie) throws Exception;
    public void updateMovie(Movie movie) throws Exception;
    public void deleteMovie(int id) throws Exception;
    public List<Movie> getMovies(MovieFilter movieFilter) throws Exception;
}
