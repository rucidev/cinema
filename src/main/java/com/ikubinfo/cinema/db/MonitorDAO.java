package com.ikubinfo.cinema.db;

import com.ikubinfo.cinema.model.monitor.Monitor;
import com.ikubinfo.cinema.model.monitor.MonitorFilter;

import java.util.List;

/**
 * Created by Aleks on May 29, 2018
 */
public interface MonitorDAO {
    public void saveMonitor(Monitor monitor) throws Exception;
    public void updateMonitor(Monitor monitor) throws Exception;
    public void deleteMonitor(int id) throws Exception;
    public List<Monitor> getMonitors(MonitorFilter monitorFilter) throws Exception;
}
