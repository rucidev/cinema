package com.ikubinfo.cinema.db;

import com.ikubinfo.cinema.model.movie.movieschedule.MovieSchedule;
import com.ikubinfo.cinema.model.movie.movieschedule.MovieScheduleFilter;

import java.util.List;

/**
 * Created by Aleks on May 29, 2018
 */
public interface MovieScheduleDAO {
    public void saveMovieSchedule(MovieSchedule movieSchedule) throws Exception;
    public void updateMovieSchedule(MovieSchedule movieSchedule) throws Exception;
    public void deleteMovieSchedule(int id) throws Exception;
    public List<MovieSchedule> getMovieSchedules(MovieScheduleFilter movieScheduleFilter) throws Exception;
}
