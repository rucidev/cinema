package com.ikubinfo.cinema.db.jdbc;

import com.ikubinfo.cinema.commons.date.ASUtilDate;
import com.ikubinfo.cinema.commons.db.jdbc.DefaultDBLayer;
import com.ikubinfo.cinema.db.UserDAO;
import com.ikubinfo.cinema.model.administrator.Administrator;
import com.ikubinfo.cinema.model.administrator.AdministratorFilter;
import com.ikubinfo.cinema.model.chair.configuredchair.ConfiguredChair;
import com.ikubinfo.cinema.model.customer.Customer;
import com.ikubinfo.cinema.model.customer.CustomerFilter;
import com.ikubinfo.cinema.model.reservation.Reservation;
import com.ikubinfo.cinema.model.user.User;
import com.ikubinfo.cinema.model.user.UserFilter;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class UserDAOImpl extends DefaultDBLayer implements UserDAO {
    public UserDAOImpl() {
        super();
    }

    public UserDAOImpl(DefaultDBLayer parentDbLayer) throws Exception {
        super(parentDbLayer);
    }

    // TODO: 4/29/2018 save, update, delete com.cinema.User methods

    public void saveUser(User user) throws Exception {
        PreparedStatement stmt = getPStmtWithGeneratedID("INSERT INTO user " +
                "(Name, " +
                "Surname," +
                "Username," +
                "Password," +
                "Mobile," +
                "Sex," +
                "Description," +
                "CDate," +
                "MDate) " +
                "VALUES ( ?,?,?,?,?,?,?,?,?);");
        int i = 1;
        stmt.setString(i++, user.getName());
        stmt.setString(i++, user.getSurname());
        stmt.setString(i++, user.getUsername());
        stmt.setString(i++, user.getPassword());
        stmt.setString(i++, user.getMobile());
        stmt.setString(i++, user.getSex());
        stmt.setString(i++, user.getDescription());
        ASUtilDate cDate = new ASUtilDate();
        stmt.setString(i++, cDate.toMySqlDateTime());
        stmt.setString(i++, cDate.toMySqlDateTime());

        stmt.executeUpdate();
        user.setId(getGeneratedID(stmt));
        stmt.close();
    }

    public void updateUser(User user) throws Exception {

    }

    public void deleteUser(int id) throws Exception {

    }

    public ArrayList<User> getUsers(UserFilter filter) throws Exception {
        ArrayList<User> users = new ArrayList<User>();
        String query = "SELECT * FROM user WHERE ";
        if (filter.isFullBody()) {
            query += "1 = 1";
        } else if (filter.getId() != MINUS_ONE) {
            query += "ID = " + filter.getId();
        } else if (filter.getUsername() != null) {
            query += "Username = '" + filter.getUsername() + "' ";
        }

        Statement stmt = getConnection().createStatement();
        ResultSet resultSet = stmt.executeQuery(query);
        while (resultSet.next()) {
            int id = resultSet.getInt("ID");
            User user = new User(id);
            user.setName(resultSet.getString("Name"));
            user.setSurname(resultSet.getString("Surname"));
            user.setUsername(resultSet.getString("Username"));
            user.setPassword(resultSet.getString("Password"));
            user.setMobile(resultSet.getString("Mobile"));
            user.setSex(resultSet.getString("Sex"));
            user.setDescription(resultSet.getString("Description"));
            user.setcDate(new ASUtilDate(resultSet.getTimestamp("CDate")));
            user.setmDate(new ASUtilDate(resultSet.getTimestamp("MDate")));
            users.add(user);
        }

        stmt.close();
        return users;
    }


    // TODO: 4/29/2018 save, update, delete Administrator methods

    //Start administrator
    public ArrayList<Administrator> getAdministrators(AdministratorFilter filter) throws Exception {
        ArrayList<Administrator> administrators = new ArrayList<Administrator>();
        String query = "SELECT  administrator.ID AS AdministratorID,\n" +
                "        administrator.UserID AS AdministratorUserID,\n" +
                "        administrator.Description AS AdministratorDescription,\n" +
                "        administrator.CDate AS AdministratorCDate,\n" +
                "        administrator.MDate AS AdministratorMDate,\n" +
                "        user.ID AS UserID,\n" +
                "        user.Name AS UserName,\n" +
                "        user.Surname AS UserSurname,\n" +
                "        user.Username AS UserUsername,\n" +
                "        user.Mobile AS UserMobile,\n" +
                "        user.Sex AS UserSex,\n" +
                "        user.Description AS UserDescription,\n" +
                "        user.CDate AS UserCDate,\n" +
                "        user.MDate AS UserMDate\n" +
                "        FROM administrator INNER JOIN user ON administrator.UserID = user.ID \n" +
                "        WHERE ";
        if (filter.isFullBody()) {
            query += "1 = 1";
        }
        if (filter.getId() != MINUS_ONE) {
            query += "administrator.ID = " + filter.getId();
        } else if (filter.getUserID() != MINUS_ONE) {
            query += "administrator.UserID = " + filter.getUserID();
        }

        Statement stmt = getConnection().createStatement();
        ResultSet resultSet = stmt.executeQuery(query);
        while (resultSet.next()) {
            int id = resultSet.getInt("AdministratorID");
            Administrator administrator = new Administrator(id);
            administrator.setDescription(resultSet.getString("AdministratorDescription"));
            administrator.setcDate(new ASUtilDate(resultSet.getTimestamp("AdministratorCDate")));
            administrator.setmDate(new ASUtilDate(resultSet.getTimestamp("AdministratorMDate")));

            User user = new User(resultSet.getInt("UserID"));
            user.setName(resultSet.getString("UserName"));
            user.setSurname(resultSet.getString("UserSurname"));
            user.setUsername(resultSet.getString("UserUsername"));
            user.setMobile(resultSet.getString("UserMobile"));
            user.setSex(resultSet.getString("UserSex"));
            user.setDescription(resultSet.getString("UserDescription"));
            user.setcDate(new ASUtilDate(resultSet.getTimestamp("UserCDate")));
            user.setmDate(new ASUtilDate(resultSet.getTimestamp("UserMDate")));
            administrator.setUser(user);

            administrators.add(administrator);
        }

        stmt.close();
        return administrators;
    }
    //End administrator

    //Start customer
    public void saveCustomer(Customer customer) throws Exception {
        PreparedStatement stmt = getPStmtWithGeneratedID("INSERT INTO customer " +
                "(UserID, " +
                "TicketsBought," +
                "Description," +
                "CDate," +
                "MDate) " +
                "VALUES (?,?,?,?,?);");
        int i = 1;
        stmt.setInt(i++, customer.getUser().getId());
        stmt.setInt(i++, customer.getTicketsBought());
        stmt.setString(i++, customer.getDescription());
        ASUtilDate cDate = new ASUtilDate();
        stmt.setString(i++, cDate.toMySqlDateTime());
        stmt.setString(i++, cDate.toMySqlDateTime());

        stmt.executeUpdate();
        customer.setId(getGeneratedID(stmt));
        stmt.close();

    }

    public ArrayList<Customer> getCustomers(CustomerFilter filter) throws Exception {
        ArrayList<Customer> customers = new ArrayList<Customer>();
        String query = "SELECT  customer.ID AS CustomerID,\n" +
                "        customer.UserID AS CustomerUserID,\n" +
                "        customer.Description AS CustomerDescription,\n" +
                "        customer.TicketsBought AS CustomerTicketsBought,\n" +
                "        customer.CDate AS CustomerCDate,\n" +
                "        customer.MDate AS CustomerMDate,\n" +
                "        user.ID AS UserID,\n" +
                "        user.Name AS UserName,\n" +
                "        user.Surname AS UserSurname,\n" +
                "        user.Username AS UserUsername,\n" +
                "        user.Mobile AS UserMobile,\n" +
                "        user.Sex AS UserSex,\n" +
                "        user.Description AS UserDescription,\n" +
                "        user.CDate AS UserCDate,\n" +
                "        user.MDate AS UserMDate\n" +
                "        FROM customer INNER JOIN user ON customer.UserID = user.ID \n" +
                "        WHERE ";
        if (filter.isFullBody()) {
            query += "1 = 1";
        }
        if (filter.getId() != MINUS_ONE) {
            query += "customer.ID = " + filter.getId();
        } else if (filter.getUserID() != MINUS_ONE) {
            query += "customer.UserID = " + filter.getUserID();
        }

        Statement stmt = getConnection().createStatement();
        ResultSet resultSet = stmt.executeQuery(query);
        while (resultSet.next()) {
            int id = resultSet.getInt("CustomerID");
            Customer customer = new Customer(id);
            customer.setTicketsBought(resultSet.getInt("CustomerTicketsBought"));
            customer.setDescription(resultSet.getString("CustomerDescription"));
            customer.setcDate(new ASUtilDate(resultSet.getTimestamp("CustomerCDate")));
            customer.setmDate(new ASUtilDate(resultSet.getTimestamp("CustomerMDate")));

            User user = new User(resultSet.getInt("UserID"));
            user.setName(resultSet.getString("UserName"));
            user.setSurname(resultSet.getString("UserSurname"));
            user.setUsername(resultSet.getString("UserUsername"));
            user.setMobile(resultSet.getString("UserMobile"));
            user.setSex(resultSet.getString("UserSex"));
            user.setDescription(resultSet.getString("UserDescription"));
            user.setcDate(new ASUtilDate(resultSet.getTimestamp("UserCDate")));
            user.setmDate(new ASUtilDate(resultSet.getTimestamp("UserMDate")));
            customer.setUser(user);

            customers.add(customer);
        }

        stmt.close();
        return customers;
    }
    //End customer

    //Start reservation

    public void saveReservation(Reservation reservation) throws Exception {
        PreparedStatement stmt = getPStmtWithGeneratedID("INSERT INTO reservation " +
                "(CustomerID, " +
                "TotalPrice," +
                "CDate) " +
                "VALUES ( ?,?,?);");
        int i = 1;
        stmt.setInt(i++, reservation.getCustomer().getId());
        stmt.setDouble(i++, reservation.getTotalPrice());
        stmt.setString(i++, new ASUtilDate().toMySqlDateTime());
        stmt.executeUpdate();
        reservation.setId(getGeneratedID(stmt));

        ArrayList<ConfiguredChair> configuredChairs = reservation.getConfiguredChairs();
        for(ConfiguredChair configuredChair: configuredChairs){
            stmt = getPStmtWithGeneratedID("INSERT INTO reservationbody " +
                    "(ReservationID, " +
                    "ConfiguredChairID) " +
                    "VALUES (?,?);");
            i = 1;
            stmt.setInt(i++, reservation.getId());
            stmt.setInt(i++, configuredChair.getId());
            stmt.executeUpdate();
        }
        stmt.close();
    }

    //End reservation

}
