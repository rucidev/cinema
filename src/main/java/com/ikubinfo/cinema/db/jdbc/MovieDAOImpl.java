package com.ikubinfo.cinema.db.jdbc;

import com.ikubinfo.cinema.commons.date.ASUtilDate;
import com.ikubinfo.cinema.commons.date.ASUtilTime;
import com.ikubinfo.cinema.commons.db.jdbc.DefaultDBLayer;
import com.ikubinfo.cinema.db.MovieDAO;
import com.ikubinfo.cinema.model.movie.EMovieType;
import com.ikubinfo.cinema.model.movie.Movie;
import com.ikubinfo.cinema.model.movie.MovieFilter;
import com.ikubinfo.cinema.model.user.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class MovieDAOImpl extends DefaultDBLayer implements MovieDAO {
    public MovieDAOImpl() {
        super();
    }

    public MovieDAOImpl(DefaultDBLayer parentDbLayer) throws Exception {
        super(parentDbLayer);
    }

    public void saveMovie(Movie movie) throws Exception{
        PreparedStatement stmt = getPStmtWithGeneratedID("INSERT INTO movie " +
                "(MovieTypeID, " +
                "Title," +
                "Producer," +
                "Duration," +
                "Description," +
                "CUserID," +
                "MUserID," +
                "CDate," +
                "MDate) " +
                "VALUES ( ?,?,?,?,?,?,?,?,?);");
        int i = 1;
        stmt.setInt(i++, movie.getMovieType().getId());
        stmt.setString(i++, movie.getTitle());
        stmt.setString(i++, movie.getProducer());
        stmt.setString(i++, movie.getDuration().toMySqlTime());
        stmt.setString(i++, movie.getDescription());
        stmt.setInt(i++, movie.getcUser().getId());
        stmt.setInt(i++, movie.getmUser().getId());
        ASUtilDate cDate = new ASUtilDate();
        stmt.setString(i++, cDate.toMySqlDateTime());
        stmt.setString(i++, cDate.toMySqlDateTime());

        stmt.executeUpdate();
        movie.setId(getGeneratedID(stmt));
        stmt.close();
    }

    public void updateMovie(Movie movie) throws Exception{
        String query = "UPDATE movie SET " +
                "MovieTypeID = ?," +
                "Title = ?," +
                "Producer = ?," +
                "Duration = ?," +
                "Description = ?," +
                "MUserID = ?," +
                "MDate = ? " +
                "WHERE ID = ?;";
        printQuery(query);

        PreparedStatement stmt = getConnection().prepareStatement(query);
        int i = 1;
        stmt.setInt(i++, movie.getMovieType().getId());
        stmt.setString(i++, movie.getTitle());
        stmt.setString(i++, movie.getProducer());
        stmt.setString(i++, movie.getDuration().toMySqlTime());
        stmt.setString(i++, movie.getDescription());
        stmt.setInt(i++, movie.getmUser().getId());
        stmt.setString(i++, new ASUtilDate().toMySqlDateTime());
        stmt.setInt(i++, movie.getId());

        stmt.executeUpdate();
        stmt.close();
    }

    public void deleteMovie(int id) throws Exception{
        String query = "DELETE FROM movie WHERE ID = ?";
        printQuery(query);

        PreparedStatement stmt = getConnection().prepareStatement(query);
        stmt.setInt(1, id);
        stmt.executeUpdate();
        stmt.close();
    }

    public List<Movie> getMovies(MovieFilter filter) throws Exception {
        ArrayList<Movie> movies = new ArrayList<Movie>();
        String query = "SELECT  movie.ID AS MovieID,\n" +
                "        movie.MovieTypeID AS MovieMovieTypeID,\n" +
                "        movie.Title AS MovieTitle,\n" +
                "        movie.Producer AS MovieProducer,\n" +
                "        movie.Duration AS MovieDuration,\n" +
                "        movie.Description AS MovieDescription,\n" +
                "        movie.CDate AS MovieCDate,\n" +
                "        movie.MDate AS MovieMDate,\n" +
                "        movietype.ID AS MovieTypeID,\n" +
                "        movietype.Name AS MovieTypeName,\n" +
                "        movietype.Description AS MovieTypeDescription,\n" +
                "        movietype.CDate AS MovieTypeCDate,\n" +
                "        movietype.MDate AS MovieTypeMDate,\n" +
                "        u1.ID AS CUserID,\n" +
                "        u1.Name AS CUserName,\n" +
                "        u1.Surname AS CUserSurname,\n" +
                "        u1.Username AS CUserUsername,\n" +
                "        u1.Mobile AS CUserMobile,\n" +
                "        u1.Sex AS CUserSex,\n" +
                "        u1.Description AS CUserDescription,\n" +
                "        u1.CDate AS CUserCDate,\n" +
                "        u1.MDate AS CUserMDate,\n" +
                "        u2.ID AS MUserID,\n" +
                "        u2.Name AS MUserName,\n" +
                "        u2.Surname AS MUserSurname,\n" +
                "        u2.Username AS MUserUsername,\n" +
                "        u2.Mobile AS MUserMobile,\n" +
                "        u2.Sex AS MUserSex,\n" +
                "        u2.Description AS MUserDescription,\n" +
                "        u2.CDate AS MUserCDate,\n" +
                "        u2.MDate AS MUserMDate\n" +
                "FROM movie INNER JOIN movietype ON movie.MovieTypeID = movietype.ID\n" +
                "        INNER JOIN user u1 ON movie.CUserID = u1.ID\n" +
                "        INNER JOIN user u2 ON movie.MUserID = u2.ID\n" +
                "        WHERE ";
        if(filter.isFullBody()){
            query += "1 = 1";
        } else if(filter.getId() != MINUS_ONE){
            query += "movie.ID = " + filter.getId();
        } else if(filter.getMovieTypeID() != MINUS_ONE){
            query += "movie.MovieTypeID = " + filter.getMovieTypeID();
        } else if(filter.getTitle() != null){
            query += "movie.Title = '" + filter.getTitle() + "' ";
        }

        Statement stmt = getConnection().createStatement();
        ResultSet resultSet = stmt.executeQuery(query);
        while(resultSet.next()) {
            int id = resultSet.getInt("MovieID");
            Movie movie = new Movie(id);
            movie.setTitle(resultSet.getString("MovieTitle"));
            movie.setProducer(resultSet.getString("MovieProducer"));
            movie.setDuration(new ASUtilTime(resultSet.getTimestamp("MovieDuration")));
            movie.setDescription(resultSet.getString("MovieDescription"));
            movie.setcDate(new ASUtilDate(resultSet.getTimestamp("MovieCDate")));
            movie.setmDate(new ASUtilDate(resultSet.getTimestamp("MovieMDate")));

            User cUser = new User(resultSet.getInt("CUserID"));
            cUser.setName(resultSet.getString("CUserName"));
            cUser.setSurname(resultSet.getString("CUserSurname"));
            cUser.setUsername(resultSet.getString("CUserUsername"));
            cUser.setMobile(resultSet.getString("CUserMobile"));
            cUser.setSex(resultSet.getString("CUserSex"));
            cUser.setDescription(resultSet.getString("CUserDescription"));
            cUser.setcDate(new ASUtilDate(resultSet.getTimestamp("CUserCDate")));
            cUser.setmDate(new ASUtilDate(resultSet.getTimestamp("CUserMDate")));
            movie.setcUser(cUser);

            User mUser = new User(resultSet.getInt("MUserID"));
            mUser.setName(resultSet.getString("MUserName"));
            mUser.setSurname(resultSet.getString("MUserSurname"));
            mUser.setUsername(resultSet.getString("MUserUsername"));
            mUser.setMobile(resultSet.getString("MUserMobile"));
            mUser.setSex(resultSet.getString("MUserSex"));
            mUser.setDescription(resultSet.getString("MUserDescription"));
            mUser.setcDate(new ASUtilDate(resultSet.getTimestamp("MUserCDate")));
            mUser.setmDate(new ASUtilDate(resultSet.getTimestamp("MUserMDate")));
            movie.setmUser(mUser);

            movie.setMovieType(EMovieType.getEnumByID(resultSet.getInt("MovieTypeID")));

            movies.add(movie);
        }

        stmt.close();
        return movies;
    }

}
