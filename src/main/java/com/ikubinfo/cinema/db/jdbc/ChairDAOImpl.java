package com.ikubinfo.cinema.db.jdbc;

import com.ikubinfo.cinema.commons.db.jdbc.DefaultDBLayer;
import com.ikubinfo.cinema.db.ChairDAO;
import com.ikubinfo.cinema.model.chair.Chair;
import com.ikubinfo.cinema.model.chair.ChairFilter;

import java.util.List;


// TODO: 5/9/2018 implement methods logic

public class ChairDAOImpl extends DefaultDBLayer implements ChairDAO {
    public ChairDAOImpl() {
        super();
    }

    public ChairDAOImpl(DefaultDBLayer parentDbLayer) throws Exception{
        super(parentDbLayer);
    }

    public void saveChair(Chair chair) {

    }

    public void updateChair(Chair chair) {

    }

    public void deleteChair(int id) {

    }

    public List<Chair> getChairs(ChairFilter chairFilter) {
        return null;
    }
}
