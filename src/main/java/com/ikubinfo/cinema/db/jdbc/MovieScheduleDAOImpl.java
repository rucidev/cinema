package com.ikubinfo.cinema.db.jdbc;

import com.ikubinfo.cinema.commons.date.ASUtilDate;
import com.ikubinfo.cinema.commons.date.ASUtilTime;
import com.ikubinfo.cinema.commons.db.jdbc.DefaultDBLayer;
import com.ikubinfo.cinema.db.MovieScheduleDAO;
import com.ikubinfo.cinema.model.monitor.Monitor;
import com.ikubinfo.cinema.model.movie.EMovieType;
import com.ikubinfo.cinema.model.movie.Movie;
import com.ikubinfo.cinema.model.movie.movieschedule.MovieSchedule;
import com.ikubinfo.cinema.model.movie.movieschedule.MovieScheduleFilter;
import com.ikubinfo.cinema.model.user.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aleks on May 29, 2018
 */
public class MovieScheduleDAOImpl  extends DefaultDBLayer implements MovieScheduleDAO {
    public MovieScheduleDAOImpl() {
        super();
    }

    public MovieScheduleDAOImpl(DefaultDBLayer parentDbLayer) throws Exception {
        super(parentDbLayer);
    }

    public void saveMovieSchedule(MovieSchedule movieSchedule) throws Exception{
        PreparedStatement stmt = getPStmtWithGeneratedID("INSERT INTO movieschedule " +
                "(MonitorID, " +
                "MovieID," +
                "Price," +
                "Description," +
                "StartTime," +
                "EndTime," +
                "CUserID," +
                "MUserID," +
                "CDate," +
                "MDate) " +
                "VALUES (?,?,?,?,?,?,?,?,?,?);");
        int i = 1;
        stmt.setInt(i++, movieSchedule.getMonitor().getId());
        stmt.setInt(i++, movieSchedule.getMovie().getId());
        stmt.setDouble(i++, movieSchedule.getPrice());
        stmt.setString(i++, movieSchedule.getDescription());
        stmt.setString(i++, movieSchedule.getStartTime().toMySqlDateTime());
        stmt.setString(i++, movieSchedule.getEndTime().toMySqlDateTime());
        stmt.setInt(i++, movieSchedule.getcUser().getId());
        stmt.setInt(i++, movieSchedule.getmUser().getId());
        ASUtilDate cDate = new ASUtilDate();
        stmt.setString(i++, cDate.toMySqlDateTime());
        stmt.setString(i++, cDate.toMySqlDateTime());

        stmt.executeUpdate();
        movieSchedule.setId(getGeneratedID(stmt));
        stmt.close();
    }

    public void updateMovieSchedule(MovieSchedule movieSchedule) throws Exception{
        String query = "UPDATE movieschedule SET " +
                "MonitorID = ?," +
                "MovieID = ?," +
                "Price = ?," +
                "Description = ?," +
                "StartTime = ?," +
                "EndTime = ?," +
                "MUserID = ?," +
                "MDate = ? " +
                "WHERE ID = ?;";
        printQuery(query);

        PreparedStatement stmt = getConnection().prepareStatement(query);
        int i = 1;
        stmt.setInt(i++, movieSchedule.getMonitor().getId());
        stmt.setInt(i++, movieSchedule.getMovie().getId());
        stmt.setDouble(i++, movieSchedule.getPrice());
        stmt.setString(i++, movieSchedule.getDescription());
        stmt.setString(i++, movieSchedule.getStartTime().toMySqlDateTime());
        stmt.setString(i++, movieSchedule.getEndTime().toMySqlDateTime());
        stmt.setInt(i++, movieSchedule.getmUser().getId());
        stmt.setString(i++, new ASUtilDate().toMySqlDateTime());
        stmt.setInt(i++, movieSchedule.getId());

        stmt.executeUpdate();
        stmt.close();
    }

    public void deleteMovieSchedule(int id) throws Exception{
        String query = "DELETE FROM movieschedule WHERE ID = ?";
        printQuery(query);

        PreparedStatement stmt = getConnection().prepareStatement(query);
        stmt.setInt(1, id);
        stmt.executeUpdate();
        stmt.close();
    }

    public List<MovieSchedule> getMovieSchedules(MovieScheduleFilter filter) throws Exception{
        ArrayList<MovieSchedule> movieSchedules = new ArrayList<MovieSchedule>();
        String query = "SELECT  movieschedule.ID AS moviescheduleID,\n" +
                "        movieschedule.MonitorID AS MoviescheduleMonitorID,\n" +
                "        movieschedule.MovieID AS MoviescheduleMovieID,\n" +
                "        movieschedule.Price AS MovieschedulePrice,\n" +
                "        movieschedule.StartTime AS MoviescheduleStartTime,\n" +
                "        movieschedule.EndTime AS MoviescheduleEndTime,\n" +
                "        movieschedule.Price AS MovieschedulePrice,\n" +
                "        movieschedule.Description AS MoviescheduleDescription,\n" +
                "        movieschedule.CDate AS MoviescheduleCDate,\n" +
                "        movieschedule.MDate AS MoviescheduleMDate,\n" +
                "        movie.ID AS MovieID,\n" +
                "        movie.MovieTypeID AS MovieMovieTypeID,\n" +
                "        movie.Title AS MovieTitle,\n" +
                "        movie.Producer AS MovieProducer,\n" +
                "        movie.Duration AS MovieDuration,\n" +
                "        movie.Description AS MovieDescription,\n" +
                "        movie.CDate AS MovieCDate,\n" +
                "        movie.MDate AS MovieMDate,\n" +
                "        movietype.ID AS MovieTypeID,\n" +
                "        movietype.Name AS MovieTypeName,\n" +
                "        movietype.Description AS MovieTypeDescription,\n" +
                "        movietype.CDate AS MovieTypeCDate,\n" +
                "        movietype.MDate AS MovieTypeMDate,\n" +
                "        monitor.ID AS MonitorID,\n" +
                "        monitor.MonitorNumber AS MonitorNumber,\n" +
                "        monitor.RoomID AS MonitorRoomID,\n" +
                "        monitor.Height AS MonitorHeight,\n" +
                "        monitor.Width AS MonitorWidth,\n" +
                "        monitor.Description AS MonitorDescription,\n" +
                "        monitor.CDate AS MonitorCDate,\n" +
                "        monitor.MDate AS MonitorMDate,\n" +
                "        u1.ID AS CUserID,\n" +
                "        u1.Name AS CUserName,\n" +
                "        u1.Surname AS CUserSurname,\n" +
                "        u1.Username AS CUserUsername,\n" +
                "        u1.Mobile AS CUserMobile,\n" +
                "        u1.Sex AS CUserSex,\n" +
                "        u1.Description AS CUserDescription,\n" +
                "        u1.CDate AS CUserCDate,\n" +
                "        u1.MDate AS CUserMDate,\n" +
                "        u2.ID AS MUserID,\n" +
                "        u2.Name AS MUserName,\n" +
                "        u2.Surname AS MUserSurname,\n" +
                "        u2.Username AS MUserUsername,\n" +
                "        u2.Mobile AS MUserMobile,\n" +
                "        u2.Sex AS MUserSex,\n" +
                "        u2.Description AS MUserDescription,\n" +
                "        u2.CDate AS MUserCDate,\n" +
                "        u2.MDate AS MUserMDate\n" +
                "FROM movieschedule INNER JOIN movie ON movieschedule.MovieID = movie.ID\n" +
                "                INNER JOIN movietype ON movie.MovieTypeID = movietype.ID\n" +
                "                INNER JOIN monitor ON movieschedule.MonitorID = monitor.ID\n" +
                "                INNER JOIN user u1 ON movie.CUserID = u1.ID\n" +
                "                INNER JOIN user u2 ON movie.MUserID = u2.ID\n" +
                "\n" +
                "\n" +
                "\n" +
                "WHERE ";
        if(filter.isFullBody()){
            query += "1 = 1";
        } else if(filter.getId() != MINUS_ONE){
            query += "movieschedule.ID = " + filter.getId();
        } else if(filter.getMonitorID() != MINUS_ONE){
            query += "movieschedule.MonitorID = " + filter.getMonitorID();
        } else if(filter.getMovieID() != MINUS_ONE){
            query += "movieschedule.MovieID = " + filter.getMovieID();
        }

        Statement stmt = getConnection().createStatement();
        ResultSet resultSet = stmt.executeQuery(query);
        while(resultSet.next()) {
            int id = resultSet.getInt("MoviescheduleID");
            MovieSchedule movieSchedule = new MovieSchedule(id);
            movieSchedule.setPrice(resultSet.getDouble("MovieschedulePrice"));
            movieSchedule.setDescription(resultSet.getString("MoviescheduleDescription"));
            movieSchedule.setStartTime(new ASUtilDate(resultSet.getTimestamp("MoviescheduleStartTime")));
            movieSchedule.setEndTime(new ASUtilDate(resultSet.getTimestamp("MoviescheduleEndTime")));
            movieSchedule.setcDate(new ASUtilDate(resultSet.getTimestamp("MoviescheduleCDate")));
            movieSchedule.setmDate(new ASUtilDate(resultSet.getTimestamp("MoviescheduleMDate")));

            User cUser = new User(resultSet.getInt("CUserID"));
            cUser.setName(resultSet.getString("CUserName"));
            cUser.setSurname(resultSet.getString("CUserSurname"));
            cUser.setUsername(resultSet.getString("CUserUsername"));
            cUser.setMobile(resultSet.getString("CUserMobile"));
            cUser.setSex(resultSet.getString("CUserSex"));
            cUser.setDescription(resultSet.getString("CUserDescription"));
            cUser.setcDate(new ASUtilDate(resultSet.getTimestamp("CUserCDate")));
            cUser.setmDate(new ASUtilDate(resultSet.getTimestamp("CUserMDate")));
            movieSchedule.setcUser(cUser);

            User mUser = new User(resultSet.getInt("MUserID"));
            mUser.setName(resultSet.getString("MUserName"));
            mUser.setSurname(resultSet.getString("MUserSurname"));
            mUser.setUsername(resultSet.getString("MUserUsername"));
            mUser.setMobile(resultSet.getString("MUserMobile"));
            mUser.setSex(resultSet.getString("MUserSex"));
            mUser.setDescription(resultSet.getString("MUserDescription"));
            mUser.setcDate(new ASUtilDate(resultSet.getTimestamp("MUserCDate")));
            mUser.setmDate(new ASUtilDate(resultSet.getTimestamp("MUserMDate")));
            movieSchedule.setmUser(mUser);


            Movie movie = new Movie(resultSet.getInt("MovieID"));
            movie.setTitle(resultSet.getString("MovieTitle"));
            movie.setProducer(resultSet.getString("MovieProducer"));
            movie.setDuration(new ASUtilTime(resultSet.getTimestamp("MovieDuration")));
            movie.setDescription(resultSet.getString("MovieDescription"));
            movie.setcDate(new ASUtilDate(resultSet.getTimestamp("MovieCDate")));
            movie.setmDate(new ASUtilDate(resultSet.getTimestamp("MovieMDate")));
            movieSchedule.setMovie(movie);

            movie.setMovieType(EMovieType.getEnumByID(resultSet.getInt("MovieTypeID")));

            Monitor monitor = new Monitor(resultSet.getInt("MonitorID"));
            monitor.setMonitorNumber(resultSet.getInt("MonitorNumber"));
            monitor.setRoomID(resultSet.getInt("MonitorRoomID"));
            monitor.setHeight(resultSet.getDouble("MonitorHeight"));
            monitor.setWidth(resultSet.getDouble("MonitorWidth"));
            monitor.setDescription(resultSet.getString("MonitorDescription"));
            monitor.setcDate(new ASUtilDate(resultSet.getTimestamp("MonitorCDate")));
            monitor.setmDate(new ASUtilDate(resultSet.getTimestamp("MonitorMDate")));
            movieSchedule.setMonitor(monitor);

            movieSchedules.add(movieSchedule);
        }

        stmt.close();
        return movieSchedules;
    }

}
