package com.ikubinfo.cinema.db.jdbc;

import com.ikubinfo.cinema.commons.date.ASUtilDate;
import com.ikubinfo.cinema.commons.date.ASUtilTime;
import com.ikubinfo.cinema.commons.db.jdbc.DefaultDBLayer;
import com.ikubinfo.cinema.db.MonitorDAO;
import com.ikubinfo.cinema.model.chair.Chair;
import com.ikubinfo.cinema.model.monitor.Monitor;
import com.ikubinfo.cinema.model.monitor.MonitorFilter;
import com.ikubinfo.cinema.model.movie.Movie;
import com.ikubinfo.cinema.model.user.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class MonitorDAOImpl extends DefaultDBLayer implements MonitorDAO {
    public MonitorDAOImpl() {
        super();
    }

    public MonitorDAOImpl(DefaultDBLayer parentDbLayer) throws Exception {
        super(parentDbLayer);
    }

    public void saveMonitor(Monitor monitor) throws Exception {
        PreparedStatement stmt = getPStmtWithGeneratedID("INSERT INTO monitor " +
                "(MonitorNumber, " +
                "RoomID," +
                "Height," +
                "Width," +
                "Description," +
                "CUserID," +
                "MUserID," +
                "CDate," +
                "MDate) " +
                "VALUES ( ?,?,?,?,?,?,?,?,?);");
        int i = 1;
        stmt.setInt(i++, monitor.getMonitorNumber());
        stmt.setInt(i++, monitor.getRoomID());
        stmt.setDouble(i++, monitor.getHeight());
        stmt.setDouble(i++, monitor.getWidth());
        stmt.setString(i++, monitor.getDescription());
        stmt.setInt(i++, monitor.getcUser().getId());
        stmt.setInt(i++, monitor.getmUser().getId());
        ASUtilDate cDate = new ASUtilDate();
        stmt.setString(i++, cDate.toMySqlDateTime());
        stmt.setString(i++, cDate.toMySqlDateTime());

        stmt.executeUpdate();
        monitor.setId(getGeneratedID(stmt));
        stmt.close();
    }

    public void updateMonitor(Monitor monitor) throws Exception {
        String query = "UPDATE monitor SET " +
                "MonitorNumber = ?," +
                "RoomID = ?," +
                "Height = ?," +
                "Width = ?," +
                "Description = ?," +
                "MUserID = ?," +
                "MDate = ? " +
                "WHERE ID = ?;";
        printQuery(query);

        PreparedStatement stmt = getConnection().prepareStatement(query);
        int i = 1;
        stmt.setInt(i++, monitor.getMonitorNumber());
        stmt.setInt(i++, monitor.getRoomID());
        stmt.setDouble(i++, monitor.getHeight());
        stmt.setDouble(i++, monitor.getWidth());
        stmt.setString(i++, monitor.getDescription());
        stmt.setInt(i++, monitor.getmUser().getId());
        stmt.setString(i++, new ASUtilDate().toMySqlDateTime());
        stmt.setInt(i++, monitor.getId());

        stmt.executeUpdate();
        stmt.close();
    }

    public void deleteMonitor(int id) throws Exception {
        String query = "DELETE FROM monitor WHERE ID = ?";
        printQuery(query);

        PreparedStatement stmt = getConnection().prepareStatement(query);
        stmt.setInt(1, id);
        stmt.executeUpdate();
        stmt.close();
    }

    public List<Monitor> getMonitors(MonitorFilter filter) throws Exception {
        ArrayList<Monitor> monitors = new ArrayList<Monitor>();
        String query = "SELECT  monitor.ID AS MonitorID,\n" +
                "        monitor.MonitorNumber AS MonitorNumber,\n" +
                "        monitor.RoomID AS MonitorRoomID,\n" +
                "        monitor.Height AS MonitorHeight,\n" +
                "        monitor.Width AS MonitorWidth,\n" +
                "        monitor.Description AS MonitorDescription,\n" +
                "        monitor.CDate AS MonitorCDate,\n" +
                "        monitor.MDate AS MonitorMDate,\n" +
                "        u1.ID AS CUserID,\n" +
                "        u1.Name AS CUserName,\n" +
                "        u1.Surname AS CUserSurname,\n" +
                "        u1.Username AS CUserUsername,\n" +
                "        u1.Mobile AS CUserMobile,\n" +
                "        u1.Sex AS CUserSex,\n" +
                "        u1.Description AS CUserDescription,\n" +
                "        u1.CDate AS CUserCDate,\n" +
                "        u1.MDate AS CUserMDate,\n" +
                "        u2.ID AS MUserID,\n" +
                "        u2.Name AS MUserName,\n" +
                "        u2.Surname AS MUserSurname,\n" +
                "        u2.Username AS MUserUsername,\n" +
                "        u2.Mobile AS MUserMobile,\n" +
                "        u2.Sex AS MUserSex,\n" +
                "        u2.Description AS MUserDescription,\n" +
                "        u2.CDate AS MUserCDate,\n" +
                "        u2.MDate AS MUserMDate\n" +
                "FROM monitor  INNER JOIN user u1 ON monitor.CUserID = u1.ID\n" +
                "              INNER JOIN user u2 ON monitor.MUserID = u2.ID\n" +
                "WHERE ";
        if (filter.isFullBody()) {
            query += "1 = 1";
        } else if (filter.getId() != MINUS_ONE) {
            query += "monitor.ID = " + filter.getId();
        } else if (filter.getRoomID() != MINUS_ONE) {
            query += "monitor.RoomID = " + filter.getRoomID();
        }

        Statement stmt = getConnection().createStatement();
        ResultSet resultSet = stmt.executeQuery(query);
        Monitor monitor = null;
        while (resultSet.next()) {
            monitor = new Monitor(resultSet.getInt("MonitorID"));
            monitor.setMonitorNumber(resultSet.getInt("MonitorNumber"));
            monitor.setRoomID(resultSet.getInt("MonitorRoomID"));
            monitor.setHeight(resultSet.getDouble("MonitorHeight"));
            monitor.setWidth(resultSet.getDouble("MonitorWidth"));
            monitor.setDescription(resultSet.getString("MonitorDescription"));
            monitor.setcDate(new ASUtilDate(resultSet.getTimestamp("MonitorCDate")));
            monitor.setmDate(new ASUtilDate(resultSet.getTimestamp("MonitorMDate")));


            User cUser = new User(resultSet.getInt("CUserID"));
            cUser.setName(resultSet.getString("CUserName"));
            cUser.setSurname(resultSet.getString("CUserSurname"));
            cUser.setUsername(resultSet.getString("CUserUsername"));
            cUser.setMobile(resultSet.getString("CUserMobile"));
            cUser.setSex(resultSet.getString("CUserSex"));
            cUser.setDescription(resultSet.getString("CUserDescription"));
            cUser.setcDate(new ASUtilDate(resultSet.getTimestamp("CUserCDate")));
            cUser.setmDate(new ASUtilDate(resultSet.getTimestamp("CUserMDate")));
            monitor.setcUser(cUser);

            User mUser = new User(resultSet.getInt("MUserID"));
            mUser.setName(resultSet.getString("MUserName"));
            mUser.setSurname(resultSet.getString("MUserSurname"));
            mUser.setUsername(resultSet.getString("MUserUsername"));
            mUser.setMobile(resultSet.getString("MUserMobile"));
            mUser.setSex(resultSet.getString("MUserSex"));
            mUser.setDescription(resultSet.getString("MUserDescription"));
            mUser.setcDate(new ASUtilDate(resultSet.getTimestamp("MUserCDate")));
            mUser.setmDate(new ASUtilDate(resultSet.getTimestamp("MUserMDate")));
            monitor.setmUser(mUser);

            monitor.setMovie(getMovie(filter, monitor));
            monitor.setChairs(getChairs(filter, monitor));

            monitors.add(monitor);
        }
        return monitors;
    }


    // TODO: 5/9/2018 merge these with appropriate methods
    private ArrayList<Chair> getChairs(MonitorFilter filter, Monitor monitor) throws Exception {
        ArrayList<Chair> chairs = new ArrayList<Chair>();
        int id = monitor.getId();
        String query = "SELECT ID FROM movieschedule " +
                "WHERE StartTime <= '" + filter.getTime().toMySqlDate() + "' " +
                "AND EndTime >= '" + filter.getTime().toMySqlDate() + "' " +
                "AND MonitorID = " + id;
        Statement stmt = getConnection().createStatement();
        ResultSet resultSet = stmt.executeQuery(query);
        while(resultSet.next()) {
            int movieTimetableID = resultSet.getInt("ID");

            query = "SELECT * FROM configuredchair " +
                    "WHERE MovieScheduleID = " + movieTimetableID;
            Statement stmt2 = getConnection().createStatement();
            ResultSet resultSet2 = stmt2.executeQuery(query);
            int chairID = -1;
            while (resultSet2.next()) {
                chairID = resultSet2.getInt("ChairID");
            }
            query = "SELECT * FROM chair " +
                    "WHERE ID = " + chairID;
            Statement stmt3 = getConnection().createStatement();
            ResultSet resultSet3 = stmt3.executeQuery(query);
            while(resultSet3.next()) {
                // TODO: 4/28/2018 set administrator and customer
                Chair chair = null;
                while (resultSet.next()) {
                    chair = new Chair(resultSet3.getInt("ID"));
                    chair.setChairNumber(resultSet3.getInt("ChairNumber"));
                    chair.setRoomID(resultSet3.getInt("RoomID"));
                    chair.setVip(resultSet3.getBoolean("IsVIP"));


                    chair.setBooked(resultSet2.getBoolean("isBooked"));
                    chair.setDescription(resultSet2.getString("Description"));
                    chair.setcDate(new ASUtilDate(resultSet2.getTimestamp("CDate")));
                    chair.setmDate(new ASUtilDate(resultSet2.getTimestamp("MDate")));

                    chairs.add(chair);
                }
            }
            stmt2.close();
            stmt3.close();
        }
        stmt.close();
        return chairs;
    }

    private Movie getMovie(MonitorFilter filter, Monitor monitor) throws Exception {
        int id = monitor.getId();
        String query = "SELECT MovieID, StartTime, EndTime FROM movieschedule " +
                "WHERE StartTime <= '" + filter.getTime().toMySqlDate() + "' " +
                "AND EndTime >= '" + filter.getTime().toMySqlDate() + "' " +
                "AND MonitorID = " + id;
        Statement stmt = getConnection().createStatement();
        ResultSet resultSet = stmt.executeQuery(query);
        Movie movie = null;
        while(resultSet.next()) {
            int movieID = resultSet.getInt("MovieID");
            ASUtilDate startTime = new ASUtilDate(resultSet.getTimestamp("StartTime"));
            ASUtilDate endTime = new ASUtilDate(resultSet.getTimestamp("EndTime"));

            String movieQuery = "SELECT * FROM movie WHERE ID = " + movieID;
            Statement stmt2 = getConnection().createStatement();
            ResultSet movieResultSet = stmt2.executeQuery(movieQuery);
            movie = new Movie(id);
            movie.setTitle(movieResultSet.getString("Tilte"));
            movie.setProducer(movieResultSet.getString("Producer"));
            movie.setPrice(movieResultSet.getDouble("Price"));
            movie.setDuration(new ASUtilTime(movieResultSet.getTimestamp("Duration")));
            movie.setStartTime(startTime);
            movie.setEndTime(endTime);
            movie.setDescription(movieResultSet.getString("Description"));
            movie.setcDate(new ASUtilDate(movieResultSet.getTimestamp("CDate")));
            movie.setmDate(new ASUtilDate(movieResultSet.getTimestamp("MDate")));

            stmt2.close();
        }
        stmt.close();
        return movie;
    }

}
