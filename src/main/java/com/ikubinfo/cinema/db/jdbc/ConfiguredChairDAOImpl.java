package com.ikubinfo.cinema.db.jdbc;

import com.ikubinfo.cinema.commons.date.ASUtilDate;
import com.ikubinfo.cinema.commons.db.jdbc.DefaultDBLayer;
import com.ikubinfo.cinema.db.ConfiguredChairDAO;
import com.ikubinfo.cinema.model.chair.configuredchair.ConfiguredChair;
import com.ikubinfo.cinema.model.chair.configuredchair.ConfiguredChairFilter;
import com.ikubinfo.cinema.model.customer.Customer;
import com.ikubinfo.cinema.model.monitor.Monitor;
import com.ikubinfo.cinema.model.movie.movieschedule.MovieSchedule;
import com.ikubinfo.cinema.model.user.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aleks on May 29, 2018
 */

// TODO: 5/9/2018 implement methods logic

public class ConfiguredChairDAOImpl extends DefaultDBLayer implements ConfiguredChairDAO {
    public ConfiguredChairDAOImpl() {
        super();
    }

    public ConfiguredChairDAOImpl(DefaultDBLayer parentDbLayer) throws Exception{
        super(parentDbLayer);
    }

    public void saveConfiguredChair(ConfiguredChair configuredChair) throws Exception {

    }

    public void updateConfiguredChair(ConfiguredChair configuredChair) throws Exception {
        String query = "UPDATE configuredchair SET " +
                "CustomerID = ?," +
                "isBooked = ?," +
                "Description = ?," +
                "MUserID = ?," +
                "MDate = ? " +
                "WHERE ID = ?;";
        printQuery(query);

        PreparedStatement stmt = getConnection().prepareStatement(query);
        int i = 1;
        stmt.setInt(i++, configuredChair.getCustomer().getId());
        stmt.setBoolean(i++, configuredChair.isBooked());
        stmt.setString(i++, configuredChair.getDescription());
        stmt.setInt(i++, configuredChair.getmUser().getId());
        stmt.setString(i++, new ASUtilDate().toMySqlDateTime());
        stmt.setInt(i++, configuredChair.getId());

        stmt.executeUpdate();
        stmt.close();
    }


    public void deleteConfiguredChair(int id) throws Exception {

    }


    public List<ConfiguredChair> getConfiguredChairs(ConfiguredChairFilter filter) throws Exception {
        ArrayList<ConfiguredChair> configuredChairs = new ArrayList<ConfiguredChair>();
        String query = "SELECT  configuredchair.ID AS ConfiguredChairID,\n" +
                "        configuredchair.ChairID AS ConfiguredChairChairID,\n" +
                "        configuredchair.MovieScheduleID AS ConfiguredChaiMovieScheduleID,\n" +
                "        configuredchair.AdministratorID AS ConfiguredChairAdministratorID,\n" +
                "        configuredchair.CustomerID AS ConfiguredChairCustomerID,\n" +
                "        configuredchair.isBooked AS ConfiguredChairIsBooked,\n" +
                "        configuredchair.Description AS ConfiguredChairDescription,\n" +
                "        configuredchair.CDate AS ConfiguredChairCDate,\n" +
                "        configuredchair.MDate AS ConfiguredChairMDate,\n" +
                "        configuredchair.Description AS ConfiguredChairDescription,\n" +
                "        configuredchair.Description AS ConfiguredChairDescription,\n" +
                "        movieschedule.ID AS MovieScheduleID,\n" +
                "        movieschedule.Price AS MovieSchedulePrice,\n" +
                "        movieschedule.MonitorID AS MovieScheduleMonitorID,\n" +
                "        movieschedule.StartTime AS MovieScheduleStartTime,\n" +
                "        chair.ID AS ChairID,\n" +
                "        chair.RoomID AS ChairRoomID,\n" +
                "        chair.ChairNumber AS ChairNumber,\n" +
                "        chair.IsVIP AS ChairIsVip,\n" +
                "        chair.Description AS ChairDescription,\n" +
                "        chair.CDate AS ChairCDate,\n" +
                "        chair.MDate AS ChairMDate,\n" +
                "        customer.ID AS CustomerID,\n" +
                "        customer.UserID AS CustomerUserID,\n" +
                "        customer.TicketsBought AS CustomerTicketsBought,\n" +
                "        customer.Description AS CustomerDescription,\n" +
                "        u1.ID AS CUserID,\n" +
                "        u1.Name AS CUserName,\n" +
                "        u1.Surname AS CUserSurname,\n" +
                "        u1.Username AS CUserUsername,\n" +
                "        u1.Mobile AS CUserMobile,\n" +
                "        u1.Sex AS CUserSex,\n" +
                "        u1.Description AS CUserDescription,\n" +
                "        u1.CDate AS CUserCDate,\n" +
                "        u1.MDate AS CUserMDate,\n" +
                "        u2.ID AS MUserID,\n" +
                "        u2.Name AS MUserName,\n" +
                "        u2.Surname AS MUserSurname,\n" +
                "        u2.Username AS MUserUsername,\n" +
                "        u2.Mobile AS MUserMobile,\n" +
                "        u2.Sex AS MUserSex,\n" +
                "        u2.Description AS MUserDescription,\n" +
                "        u2.CDate AS MUserCDate,\n" +
                "        u2.MDate AS MUserMDate\n" +
                "FROM configuredchair INNER JOIN chair ON configuredchair.ChairID = chair.ID\n" +
                "                INNER JOIN movieschedule ON configuredchair.MovieScheduleID = movieschedule.ID\n" +
                "                LEFT JOIN customer ON configuredchair.CustomerID = customer.ID\n" +
                "                INNER JOIN user u1 ON configuredchair.CUserID = u1.ID\n" +
                "                INNER JOIN user u2 ON configuredchair.CUserID = u2.ID\n" +
                "WHERE ";
        if (filter.isFullBody()) {
            query += "1 = 1";
        } else if (filter.getId() != MINUS_ONE) {
            query += "configuredchair.ID = " + filter.getId();
        }
        Statement stmt = getConnection().createStatement();
        ResultSet resultSet = stmt.executeQuery(query);
        ConfiguredChair configuredChair = null;
        while (resultSet.next()) {
            configuredChair = new ConfiguredChair(resultSet.getInt("ConfiguredChairID"));
            configuredChair.setBooked(resultSet.getBoolean("ConfiguredChairIsBooked"));
            configuredChair.setDescription(resultSet.getString("ConfiguredChairDescription"));
            configuredChair.setcDate(new ASUtilDate(resultSet.getTimestamp("ConfiguredChairCDate")));
            configuredChair.setmDate(new ASUtilDate(resultSet.getTimestamp("ConfiguredChairMDate")));

            MovieSchedule movieSchedule = new MovieSchedule(resultSet.getInt("MovieScheduleID"));
            movieSchedule.setMonitor(new Monitor(resultSet.getInt("MovieScheduleMonitorID")));
            movieSchedule.setPrice(resultSet.getDouble("MovieSchedulePrice"));
            movieSchedule.setStartTime(new ASUtilDate(resultSet.getTimestamp("MovieScheduleStartTime")));
            configuredChair.setMovieSchedule(movieSchedule);

            Customer customer = new Customer(resultSet.getInt("CustomerID"));
            customer.setTicketsBought(resultSet.getInt("CustomerTicketsBought"));
            customer.setUser(new User(resultSet.getInt("CustomerUserID")));
            customer.setDescription(resultSet.getString("CustomerDescription"));
            configuredChair.setCustomer(customer);

            User cUser = new User(resultSet.getInt("CUserID"));
            cUser.setName(resultSet.getString("CUserName"));
            cUser.setSurname(resultSet.getString("CUserSurname"));
            cUser.setUsername(resultSet.getString("CUserUsername"));
            cUser.setMobile(resultSet.getString("CUserMobile"));
            cUser.setSex(resultSet.getString("CUserSex"));
            cUser.setDescription(resultSet.getString("CUserDescription"));
            cUser.setcDate(new ASUtilDate(resultSet.getTimestamp("CUserCDate")));
            cUser.setmDate(new ASUtilDate(resultSet.getTimestamp("CUserMDate")));
            configuredChair.setcUser(cUser);

            User mUser = new User(resultSet.getInt("MUserID"));
            mUser.setName(resultSet.getString("MUserName"));
            mUser.setSurname(resultSet.getString("MUserSurname"));
            mUser.setUsername(resultSet.getString("MUserUsername"));
            mUser.setMobile(resultSet.getString("MUserMobile"));
            mUser.setSex(resultSet.getString("MUserSex"));
            mUser.setDescription(resultSet.getString("MUserDescription"));
            mUser.setcDate(new ASUtilDate(resultSet.getTimestamp("MUserCDate")));
            mUser.setmDate(new ASUtilDate(resultSet.getTimestamp("MUserMDate")));
            configuredChair.setmUser(mUser);

            configuredChairs.add(configuredChair);
        }
        return configuredChairs;
    }
}
