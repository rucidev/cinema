package com.ikubinfo.cinema.db.hibernate;

import com.ikubinfo.cinema.commons.db.hibernate.DefaultHibernateLayer;
import com.ikubinfo.cinema.commons.db.jdbc.DefaultDBLayer;
import com.ikubinfo.cinema.db.MovieDAO;
import com.ikubinfo.cinema.model.movie.Movie;
import com.ikubinfo.cinema.model.movie.Movie1;
import com.ikubinfo.cinema.model.movie.MovieFilter;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by Aleks on Jun 06, 2018
 */
public class MovieDAOHibernate extends DefaultHibernateLayer implements MovieDAO {
    public MovieDAOHibernate() {
        super();
    }

    public MovieDAOHibernate(DefaultHibernateLayer parentDbLayer) throws Exception {
        super(parentDbLayer);
    }

    public void saveMovie1(Movie1 movie1) throws Exception{
        EntityManager em = getEntityManager();
        em.persist(movie1);
    }

    public void saveMovie(Movie movie) throws Exception {
        EntityManager em = getEntityManager();
        em.persist(movie);
    }

    public void updateMovie(Movie movie) throws Exception {

    }

    public void deleteMovie(int id) throws Exception {

    }

    public List<Movie> getMovies(MovieFilter movieFilter) throws Exception {
        return null;
    }
}
