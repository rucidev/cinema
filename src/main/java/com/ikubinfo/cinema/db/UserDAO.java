package com.ikubinfo.cinema.db;

import com.ikubinfo.cinema.model.user.User;
import com.ikubinfo.cinema.model.user.UserFilter;

import java.util.List;

/**
 * Created by Aleks on May 29, 2018
 */
public interface UserDAO {
    public void saveUser(User user) throws Exception;
    public void updateUser(User user) throws Exception;
    public void deleteUser(int id) throws Exception;
    public List<User> getUsers(UserFilter userFilter) throws Exception;
}
