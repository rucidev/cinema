package com.ikubinfo.cinema.algorithm;

public class CinemaConstants {
    public static final String OPENING_TIME = "08:00:00";
    public static final String CLOSING_TIME = "02:00:00";
    public static final double CHAIR_CONFIGURATION_PRICE = 200;
}
