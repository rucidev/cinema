package com.ikubinfo.cinema.algorithm;


import com.ikubinfo.cinema.model.chair.configuredchair.ConfiguredChair;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class ReservationAlgorithm {
    public static double calculateTotalPriceOfReservation(ArrayList<ConfiguredChair> configuredChairs) {
        double totalPrice = 0;
        Set<Integer> monitorIdsSet = new HashSet<Integer>();
        for(ConfiguredChair configuredChair: configuredChairs){
            totalPrice += configuredChair.getMovieSchedule().getPrice();
            monitorIdsSet.add(configuredChair.getMovieSchedule().getMonitor().getId());
        }
        if(monitorIdsSet.size() > 1){
            totalPrice += CinemaConstants.CHAIR_CONFIGURATION_PRICE;
        }
        return totalPrice;
    }
}
