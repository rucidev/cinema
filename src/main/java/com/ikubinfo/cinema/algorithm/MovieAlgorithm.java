package com.ikubinfo.cinema.algorithm;

import com.ikubinfo.cinema.commons.date.ASUtilDate;
import com.ikubinfo.cinema.logic.manager.SystemManager;
import com.ikubinfo.cinema.model.movie.movieschedule.MovieSchedule;

import java.util.ArrayList;
import java.util.Calendar;

public class MovieAlgorithm {
    public static void checkMovieScheduleOverlap(MovieSchedule movieSchedule1) throws RuntimeException {
        if (isClosed(movieSchedule1)) {
            throw new RuntimeException("Cinema is closed at that time!");
        }
        ArrayList<MovieSchedule> movieSchedules = null;
        try{
            movieSchedules = (ArrayList<MovieSchedule>) new SystemManager().getAllMovieTimeTables();
        } catch (Exception e){
            e.printStackTrace();
        }
        for (MovieSchedule movieSchedule2 : movieSchedules) {
            if(movieSchedule1.getMonitor().getId() == movieSchedule2.getMonitor().getId()){
                if (movieSchedule2.getId() != movieSchedule1.getId() && hasOverlap(movieSchedule1.getStartTime(), movieSchedule1.getEndTime(), movieSchedule2.getStartTime(), movieSchedule2.getEndTime())) {
                    throw new RuntimeException("There is monitor-movie time overlap at that time!");
                }
            }
        }
    }

    public static boolean isClosed(MovieSchedule movie1) {
        int openTime = Integer.parseInt(CinemaConstants.OPENING_TIME.substring(0, 2) + CinemaConstants.OPENING_TIME.substring(3, 5));
        int closingTime = Integer.parseInt(CinemaConstants.CLOSING_TIME.substring(0, 2) + CinemaConstants.CLOSING_TIME.substring(3, 5));

        boolean isStartBetween = isHourMinuteBetween(movie1.getStartTime(), openTime, closingTime);
        boolean isEndBetween = isHourMinuteBetween(movie1.getEndTime(), openTime, closingTime);

        Calendar calendar = Calendar.getInstance();
        if(closingTime <= 1200){
            calendar.setTime(movie1.getEndTime());
        } else{
            calendar.setTime(movie1.getStartTime());
        }
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE),
                Integer.parseInt(CinemaConstants.CLOSING_TIME.substring(0, 2)), Integer.parseInt(CinemaConstants.CLOSING_TIME.substring(3, 5)), 0);

//        System.out.println(calendar.getTime());
        if(hasOverlap(movie1.getStartTime(), movie1.getEndTime(), new ASUtilDate(calendar.getTime()), new ASUtilDate(calendar.getTime()))){
            return true;
        }

        return !(isStartBetween && isEndBetween);
    }

    private static boolean hasOverlap(ASUtilDate startDate1 , ASUtilDate endDate1, ASUtilDate startDate2, ASUtilDate endDate2) {
        boolean b1 = endDate1.after(startDate2);
        boolean b2 = endDate2.after(startDate1);
        return b1 && b2;
    }

    private static boolean isHourMinuteBetween(ASUtilDate time, int openTime, int closingTime) {
        Calendar c = Calendar.getInstance();
        c.setTime(time);
        int start = c.get(Calendar.HOUR_OF_DAY) * 100 + c.get(Calendar.MINUTE);
        return closingTime > openTime && start >= openTime && start <= closingTime || closingTime < openTime && (start >= openTime || start <= closingTime);
    }
}
