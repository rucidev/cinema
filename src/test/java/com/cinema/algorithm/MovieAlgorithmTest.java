package com.cinema.algorithm;

import com.ikubinfo.cinema.algorithm.MovieAlgorithm;
import com.ikubinfo.cinema.commons.date.ASUtilDate;
import com.ikubinfo.cinema.model.movie.movieschedule.MovieSchedule;
import org.junit.Test;

import static org.junit.Assert.*;

public class MovieAlgorithmTest {
    MovieSchedule movieSchedule = new MovieSchedule();

    @Test
    public void TestIsClosed() {
        test("2018-05-10 20:00:00", "2018-05-11 03:00:00", true);
        test("2018-05-10 19:00:00", "2018-05-11 01:00:00", false);
        test("2018-05-10 20:00:00", "2018-05-11 03:00:00", true);
        test("2018-05-15 07:00:00", "2018-05-15 15:00:00", true);
        test("2018-05-10 20:00:00", "2018-05-10 23:00:00", false);
        test("2018-05-10 20:00:00", "2018-05-11 00:00:00", false);
    }

    private void test(String startTime, String endTime, boolean flag) {
        try {
            movieSchedule.setStartTime(new ASUtilDate(startTime));
            movieSchedule.setEndTime(new ASUtilDate(endTime));
        }catch (Exception e){ }
        if(flag){
            assertTrue(MovieAlgorithm.isClosed(movieSchedule));
        } else{
            assertFalse(MovieAlgorithm.isClosed(movieSchedule));
        }
    }

    private MovieSchedule getMovieSchedule(String startTime, String endTime, int monitorID) {
        MovieSchedule movieSchedule = new MovieSchedule();
        try {
            movieSchedule.setStartTime(new ASUtilDate(startTime));
            movieSchedule.setEndTime(new ASUtilDate(endTime));
        }catch (Exception e){ }
        return movieSchedule;
    }
}